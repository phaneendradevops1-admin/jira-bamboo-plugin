# MONOREPO #

This plugin was monorepoed to Jira Software Application. All development for Jira 8.3 onwards should start in [the new repository](https://stash.atlassian.com/projects/JSWSERVER/repos/jira-software-application/browse/jira-software-plugins/jira-bamboo-plugin).

# What Is It?

A Jira plugin for integrating with Bamboo.

# Who Owns It?

The Bamboo team is responsible for doing releases, fixing bugs, etc.

# Resources

* Ask in the Bamboo room on Stride
* See [HAN](https://hello.atlassian.net/wiki/spaces/BAMBOO/pages/141593134/JIRA+Bamboo+Plugin) for more details.

# Builds

There are two separate build plans with different purposes:

* A build [on SerBac](https://server-gdn-bamboo.internal.atlassian.com/browse/PLUGINS-JBPT). runs verification tests, use for releasing
* A build [on Tardigrade](https://tardigrade-bamboo.internal.atlassian.com/browse/NEW-IMPORTANT-JBAMTEST) for testing JBAM with different branches of Bamboo. Do not create feature branches of this plan if you are making changes to JBAM.


URI.js was generated in http://medialize.github.io/URI.js/build.html with URI.js option only.

# Running

For development purposes, you can run this plugin from the command line via AMPS as normal:

    mvn amps:debug -DinstanceId=jira -DskipTests

To run the CTK tests from your IDE, first start Jira as follows:

    mvn amps:debug -DinstanceId=jira-clean -DskipTests

To install the artifact and run unit tests only:

    mvn clean install -DskipITs

To run unit tests only

    mvn test
