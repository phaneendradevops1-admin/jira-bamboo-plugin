package com.atlassian.jira.plugin.ext.bamboo.service;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.AuthenticationContextImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.security.Principal;

@Component
public class ImpersonationServiceImpl implements ImpersonationService {
    private final UserManager userManager;
    private final com.atlassian.sal.api.user.UserManager salUserManager;

    @Autowired
    public ImpersonationServiceImpl(
            @ComponentImport("jiraUserManager") final UserManager userManager,
            @ComponentImport("salUserManager") final com.atlassian.sal.api.user.UserManager salUserManager) {
        this.salUserManager = salUserManager;
        this.userManager = userManager;
    }

    @Nonnull
    public Runnable runAsUser(@Nonnull final String username, @Nonnull final Runnable delegate) {
        return () -> {
            AuthenticationContext authenticationContext = new AuthenticationContextImpl();
            Principal currentPrincipal = authenticationContext.getUser();
            try {
                final ApplicationUser user = userManager.getUserByName(username);
                if (user == null) {
                    throw new IllegalStateException("username '" + username + "' does not exist. Cannot impersonate this user.");
                }

                authenticationContext.setUser(user);

                String remoteUserName = salUserManager.getRemoteUser() != null ?
                        salUserManager.getRemoteUser().getUsername() : null;
                if (!username.equals(remoteUserName)) {
                    throw new IllegalStateException("Could not impersonate user '" + username + "'. Call to '" +
                            salUserManager.getClass() + ".getRemoteUsername()' returns '" + remoteUserName + "'");
                }

                delegate.run();
            } finally {
                authenticationContext.setUser(currentPrincipal);
            }
        };
    }
}
