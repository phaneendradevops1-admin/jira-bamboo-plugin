package com.atlassian.jira.plugin.ext.bamboo.service;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.jira.plugin.ext.bamboo.model.BambooDeploymentProject;
import com.atlassian.jira.plugin.ext.bamboo.model.BambooEnvironment;
import com.atlassian.jira.plugin.ext.bamboo.model.BambooPlan;
import com.atlassian.jira.plugin.ext.bamboo.model.BambooProject;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKey;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKeys;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultKey;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultStatus;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanStatus;
import com.atlassian.jira.plugin.ext.bamboo.model.RestResult;
import com.atlassian.jira.plugin.ext.bamboo.server.BambooServerInfo;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.URLCodec;
import com.atlassian.jira.util.UrlBuilder;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.MediaType;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BAMBOO_ERROR_CONNECTIVITY_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.DEFAULT_SERVER_NAME;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.net.HttpHeaders.CONTENT_TYPE;

@ExportAsService(BambooRestService.class)
@Component
public class BambooRestServiceImpl implements BambooRestService {
    private static final Logger log = Logger.getLogger(BambooRestServiceImpl.class);

    public static final int TRIGGER_CALL_TIMEOUT = 50000; //in miliseconds, this should be less than default Jira AJAX call timeout of 60000
    public static final String X_ATLASSIAN_TOKEN = "X-Atlassian-Token";
    public static final String NO_CHECK = "no-check";

    private final JiraAuthenticationContext authenticationContext;

    @Autowired
    public BambooRestServiceImpl(@ComponentImport final JiraAuthenticationContext authenticationContext) {
        this.authenticationContext = checkNotNull(authenticationContext);
    }

    @Nonnull
    public RestResult<Map<BambooProject, List<BambooPlan>>> getPlanList(
            @Nonnull final ApplicationLink applicationLink, final boolean includeDisabled)
            throws CredentialsRequiredException {
        final String plansUrl = "/rest/api/latest/project.json?expand=projects.project.plans&withBuildPermissionOnly&max-results=10000";
        final BambooRestResponse response = executeGetRequest(applicationLink, plansUrl, TRIGGER_CALL_TIMEOUT);

        final Collection<String> errorCollection = new HashSet<String>();
        final Map<BambooProject, List<BambooPlan>> projectPlansMap = new TreeMap<BambooProject, List<BambooPlan>>();
        try {
            final JSONObject result = parseResponseForJsonObject(applicationLink.getName(), response, errorCollection);
            if (result != null) {
                final JSONArray projectArray = result.getJSONObject("projects").getJSONArray("project");
                for (int i = 0; i < projectArray.length(); i++) {
                    final JSONObject projectObject = projectArray.getJSONObject(i);
                    String projectName = projectObject.getString("name");
                    String projectKey = projectObject.getString("key");
                    BambooProject project = new BambooProject(projectKey, projectName);

                    List<BambooPlan> plans = new ArrayList<BambooPlan>();
                    JSONArray planArray = projectObject.getJSONObject("plans").getJSONArray("plan");
                    for (int j = 0; j < planArray.length(); j++) {
                        final JSONObject planObject = planArray.getJSONObject(j);
                        String planName = planObject.getString("name");
                        String shortName = planObject.getString("shortName");
                        String planKey = planObject.getString("key");
                        boolean enabled = planObject.getBoolean("enabled");
                        if (enabled || includeDisabled) {
                            plans.add(new BambooPlan(planKey, planName, shortName));
                        }
                    }
                    Collections.sort(plans);
                    if (!plans.isEmpty()) {
                        projectPlansMap.put(project, plans);
                    }
                }
            }
        } catch (JSONException e) {
            log.error("Failed to parse Bamboo plan list", e);
            errorCollection.add(getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, applicationLink.getName()));
        }

        return new RestResult<>(projectPlansMap, errorCollection, response.getStatusCode());
    }

    @Nonnull
    public RestResult<PlanResultStatus> getPlanResultStatus(
            @Nonnull final ApplicationLinkRequestFactory applicationLinkRequestFactory, @Nonnull final PlanResultKey planResultKey)
            throws CredentialsRequiredException {
        final String resultUrl = "/rest/api/latest/result/" + planResultKey.getPlanKey().getKey() + "/" + planResultKey.getBuildNumber() + ".json";
        final BambooRestResponse response =
                executeGetRequest(applicationLinkRequestFactory, resultUrl, TRIGGER_CALL_TIMEOUT, DEFAULT_SERVER_NAME);

        final Collection<String> errorCollection = new HashSet<String>();
        try {
            JSONObject result = parseResponseForJsonObject(DEFAULT_SERVER_NAME, response, errorCollection);
            if (result != null) {
                return new RestResult<>(PlanResultStatus.fromJsonObject(planResultKey, result), errorCollection, response.getStatusCode());
            }
        } catch (JSONException e) {
            log.error("Failed to parse Bamboo result status", e);
            errorCollection.add(getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, DEFAULT_SERVER_NAME));
        }

        final PlanResultStatus planResultStatus = new PlanResultStatus(planResultKey, null, null, false);
        return new RestResult<>(planResultStatus, errorCollection, response.getStatusCode());
    }

    @Nonnull
    public RestResult<PlanStatus> getPlanStatus(
            @Nonnull ApplicationLinkRequestFactory applicationLinkRequestFactory, @Nonnull PlanKey planKey)
            throws CredentialsRequiredException {
        final String resultUrl = "/rest/api/latest/result/" + planKey.getKey() + ".json?max-results=1";
        final BambooRestResponse response =
                executeGetRequest(applicationLinkRequestFactory, resultUrl, TRIGGER_CALL_TIMEOUT, DEFAULT_SERVER_NAME);

        final Collection<String> errorCollection = new HashSet<String>();
        try {
            JSONObject results = parseResponseForJsonObject(DEFAULT_SERVER_NAME, response, errorCollection);
            if (results != null) {
                JSONArray resultsArray = results.getJSONObject("results").getJSONArray("result");
                if (resultsArray.isNull(0)) {
                    return new RestResult<PlanStatus>(new PlanStatus(planKey, null, null), errorCollection, response.getStatusCode());
                } else {
                    JSONObject latestResult = resultsArray.getJSONObject(0);
                    return new RestResult<PlanStatus>(PlanStatus.fromJsonObject(planKey, latestResult), errorCollection, response.getStatusCode());
                }
            }
        } catch (JSONException e) {
            log.error("Failed to parse Bamboo latest result status for Plan " + planKey + ": ", e);
            errorCollection.add(getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, DEFAULT_SERVER_NAME));
        }

        return new RestResult<>(null, errorCollection, response.getStatusCode());
    }

    @Nonnull
    public RestResult<JSONObject> getPlanResultJson(
            @Nonnull final ApplicationLinkRequestFactory applicationLinkRequestFactory, @Nonnull final PlanResultKey planResultKey)
            throws CredentialsRequiredException {
        final String resultUrl = "/rest/api/latest/result/" + planResultKey.getPlanKey().getKey() + "/" + planResultKey.getBuildNumber() + ".json?expand=artifacts.artifact,labels.label";
        final BambooRestResponse response =
                executeGetRequest(applicationLinkRequestFactory, resultUrl, TRIGGER_CALL_TIMEOUT, DEFAULT_SERVER_NAME);

        final Collection<String> errorCollection = Sets.newHashSet();
        final JSONObject jsonObject = parseResponseForJsonObject(DEFAULT_SERVER_NAME, response, errorCollection);
        return new RestResult<>(jsonObject, errorCollection, response.getStatusCode());
    }

    @Nonnull
    public RestResult<JSONObject> getPlanJson(
            @Nonnull final ApplicationLinkRequestFactory applicationLinkRequestFactory, @Nonnull final PlanKey planKey)
            throws CredentialsRequiredException {
        final String resultUrl = "/rest/api/latest/plan/" + planKey + ".json?expand=stages,variableContext";
        final BambooRestResponse response =
                executeGetRequest(applicationLinkRequestFactory, resultUrl, TRIGGER_CALL_TIMEOUT, DEFAULT_SERVER_NAME);

        final Collection<String> errorCollection = Sets.newHashSet();
        final JSONObject jsonObject = parseResponseForJsonObject(DEFAULT_SERVER_NAME, response, errorCollection);
        return new RestResult<>(jsonObject, errorCollection, response.getStatusCode());
    }

    public RestResult<JSONObject> getPlanHistory(@Nonnull final ApplicationLinkRequestFactory applicationLinkRequestFactory,
                                                 @Nonnull final PlanKey planKey, final int numberOfResults, @Nonnull Map<String, String> urlParams)
            throws CredentialsRequiredException {
        if (numberOfResults < 0) {
            throw new IllegalArgumentException("Cannot request zero or negative number of results");
        }

        final StringBuilder resultUrl = new StringBuilder("/rest/api/latest/result/" + planKey + ".json?expand=results.result.stages&max-results=" + numberOfResults);

        for (final Map.Entry<String, String> entry : urlParams.entrySet()) {
            resultUrl.append('&')
                    .append(entry.getKey())
                    .append('=')
                    .append(entry.getValue());
        }

        final BambooRestResponse response = executeGetRequest(
                applicationLinkRequestFactory, resultUrl.toString(), TRIGGER_CALL_TIMEOUT, DEFAULT_SERVER_NAME);

        final Collection<String> errorCollection = Sets.newHashSet();

        final JSONObject jsonObject = parseResponseForJsonObject(DEFAULT_SERVER_NAME, response, errorCollection);
        return new RestResult<>(jsonObject, errorCollection, response.getStatusCode());
    }

    @Nonnull
    public RestResult<PlanResultKey> triggerPlan(@Nonnull final ApplicationLink applicationLink,
                                                 @Nonnull final PlanKey planKey, @Nullable final String stage, @Nonnull final Map<String, String> params)
            throws CredentialsRequiredException {
        return executeTrigger(applicationLink, planKey.getKey(), stage, params, TriggerType.NEW);
    }

    @Nonnull
    public RestResult<PlanResultKey> continuePlan(@Nonnull final ApplicationLink applicationLink,
                                                  @Nonnull final PlanResultKey planResultKey, @Nullable final String stage, @Nonnull final Map<String, String> params)
            throws CredentialsRequiredException {
        return executeTrigger(applicationLink, planResultKey.getKey(), stage, params, TriggerType.EXISTING);
    }

    @Override
    public RestResult<JSONObject> getDeploymentProjectsForIssue(
            @Nonnull final ApplicationLinkRequestFactory applicationLinkRequestFactory, @Nonnull final String serverName,
            @Nonnull final String issueKey)
            throws CredentialsRequiredException {
        final String resultUrl = "/rest/api/latest/deploy/issue-status/" + issueKey;
        final BambooRestResponse response = executeGetRequest(applicationLinkRequestFactory, resultUrl, TRIGGER_CALL_TIMEOUT, serverName);
        final Collection<String> errorCollection = new HashSet<String>();
        final JSONObject jsonObject = parseResponseForJsonObject(serverName, response, errorCollection, false);
        return new RestResult<>(jsonObject, errorCollection, response.getStatusCode());
    }


    @Override
    public RestResult<JSONObject> getIssueDeploymentStatus(@Nonnull final ApplicationLink applicationLink,
                                                           @Nonnull final String issueKey, @Nonnull final String deploymentProjectId)
            throws CredentialsRequiredException {
        final String resultUrl = "/rest/api/latest/deploy/issue-status/" + issueKey + "/" + deploymentProjectId;
        final BambooRestResponse response = executeGetRequest(
                applicationLink.createAuthenticatedRequestFactory(), resultUrl, TRIGGER_CALL_TIMEOUT, DEFAULT_SERVER_NAME);
        final Collection<String> errorCollection = new HashSet<String>();
        final JSONObject jsonObject = parseResponseForJsonObject(applicationLink.getName(), response, errorCollection, false);
        return new RestResult<>(jsonObject, errorCollection, response.getStatusCode());
    }

    @Override
    public RestResult<BambooServerInfo> getBambooServerInfo(@Nonnull final ApplicationLinkRequestFactory applicationLinkRequestFactory)
            throws CredentialsRequiredException {
        final String resultUrl = "/rest/api/latest/info.json";
        final BambooRestResponse response =
                executeGetRequest(applicationLinkRequestFactory, resultUrl, TRIGGER_CALL_TIMEOUT, DEFAULT_SERVER_NAME);
        final Collection<String> errorCollection = Sets.newHashSet();
        final JSONObject jsonObject = parseResponseForJsonObject(DEFAULT_SERVER_NAME, response, errorCollection);
        return new RestResult<>(BambooServerInfo.fromJSON(jsonObject, errorCollection), errorCollection, response.getStatusCode());
    }

    @Nonnull
    @Override
    public RestResult<Map<BambooDeploymentProject, List<BambooEnvironment>>> getEnvironmentList(@Nonnull final ApplicationLink applicationLink)
            throws CredentialsRequiredException {
        final String restUrl = "/rest/api/latest/deploy/project/all.json";
        final BambooRestResponse response = executeGetRequest(applicationLink, restUrl, TRIGGER_CALL_TIMEOUT);

        final Collection<String> errorCollection = new HashSet<String>();
        final Map<BambooDeploymentProject, List<BambooEnvironment>> projectEnvironmentsMap = Maps.newTreeMap();

        try {
            final JSONArray deploymentProjects = parseResponseForJsonArray(applicationLink.getName(), response, errorCollection, true);
            if (deploymentProjects != null) {
                for (int i = 0; i < deploymentProjects.length(); i++) {
                    final JSONObject projectObject = deploymentProjects.getJSONObject(i);
                    final BambooDeploymentProject deploymentProject = parseBambooDeploymentProject(projectObject);

                    final List<BambooEnvironment> environments = Lists.newArrayList();
                    final JSONArray environmentArray = projectObject.getJSONArray("environments");
                    for (int j = 0; j < environmentArray.length(); j++) {
                        final JSONObject environmentObject = environmentArray.getJSONObject(j);
                        final BambooEnvironment environment = parseBambooEnvironment(environmentObject);
                        environments.add(environment);
                    }
                    projectEnvironmentsMap.put(deploymentProject, environments);
                }
            }
        } catch (JSONException e) {
            log.error("Failed to parse Bamboo Deployment Project list", e);
            errorCollection.add(getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, applicationLink.getName()));
        }

        return new RestResult(projectEnvironmentsMap, errorCollection, response.getStatusCode());
    }

    private BambooDeploymentProject parseBambooDeploymentProject(final JSONObject projectObject) throws JSONException {
        final long projectId = projectObject.getLong("id");
        final String projectKey = projectObject.getJSONObject("key").getString("key");
        final String projectName = projectObject.getString("name");
        return new BambooDeploymentProject(projectId, projectKey, projectName);
    }

    private BambooEnvironment parseBambooEnvironment(final JSONObject environmentObject) throws JSONException {
        final long environmentId = environmentObject.getLong("id");
        final String environmentKey = environmentObject.getJSONObject("key").getString("key");
        final String environmentName = environmentObject.getString("name");

        return new BambooEnvironment(environmentId, environmentKey, environmentName);
    }

    private RestResult<PlanResultKey> executeTrigger(final ApplicationLink applicationLink, final String planKey,
                                                     final String stage, final Map<String, String> params, final TriggerType triggerType)
            throws CredentialsRequiredException {
        final Collection<String> errorCollection = new HashSet<String>();

        String url = "/rest/api/latest/queue/" + planKey + ".json";
        if (StringUtils.isNotBlank(stage)) {
            try {
                url = url + "?stage=" + URLCodec.encode(stage, true);
            } catch (UnsupportedEncodingException e) {
                errorCollection.add(getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, applicationLink.getName()));
                log.error("Failed to trigger release build, requested stage name '" + stage + "' could not be encoded. ", e);
            }
        }

        BambooRestResponse response = (triggerType == TriggerType.NEW)
                ? executePostRequest(applicationLink, url, params, TRIGGER_CALL_TIMEOUT)
                : executePutRequest(applicationLink, url, params, TRIGGER_CALL_TIMEOUT, applicationLink.getName());

        JSONObject result = parseResponseForJsonObject(applicationLink.getName(), response, errorCollection);
        if (result != null) {
            try {
                String planResultKey = result.getString("buildResultKey");
                if (StringUtils.isNotBlank(planResultKey)) {
                    return new RestResult<PlanResultKey>(PlanKeys.getPlanResultKey(planResultKey), errorCollection, response.getStatusCode());
                } else {
                    errorCollection.add(getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, applicationLink.getName()));
                    log.error("Could not parse plan result key from Bamboo response: " + response.getResponseBody());
                }
            } catch (JSONException e) {
                errorCollection.add(getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, applicationLink.getName()));
                log.error("Failed to trigger build, could not parse response from Bamboo", e);
            }
        }

        return new RestResult<PlanResultKey>(null, errorCollection, response.getStatusCode());
    }

    @Nullable
    private JSONObject parseResponseForJsonObject(String serverName, BambooRestResponse response, Collection<String> errorCollection) {
        return parseResponseForJsonObject(serverName, response, errorCollection, true);
    }

    /**
     * Attempts to retrieve a json object from the response.  Checks for appropriate error codes and formatting in response.
     * Any errors will be added to the error collection.
     * This method should not a null {@link RestResult#getResult()} without adding a reason in the error collection
     *
     * @param response                   bamboo wrapped http response
     * @param errorCollection            collection to add errors to
     * @param includeResponseBodyInError whether to include the response body in the error
     * @return the high level json object retrieved from the response body.
     */
    @Nullable
    private JSONObject parseResponseForJsonObject(final String serverName, final BambooRestResponse response,
                                                  final Collection<String> errorCollection, final boolean includeResponseBodyInError) {
        if (isResponseValid(serverName, response, errorCollection, includeResponseBodyInError)) {
            String responseBody = response.getResponseBody();
            if (responseBody != null && !responseBody.isEmpty()) {
                try {
                    return new JSONObject(responseBody);
                } catch (JSONException e) {
                    errorCollection.add(getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, serverName));
                    log.error("Bamboo Rest Request failed, could not parse response from " + serverName, e);
                }
            } else {
                errorCollection.add(getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, serverName));
                log.error("Bamboo Rest Request failed, no response received from " + serverName);
            }
        }

        return null;
    }

    private JSONArray parseResponseForJsonArray(final String serverName, final BambooRestResponse response,
                                                final Collection<String> errorCollection, final boolean includeResponseBodyInError) {
        if (isResponseValid(serverName, response, errorCollection, includeResponseBodyInError)) {
            String responseBody = response.getResponseBody();
            if (responseBody != null && !responseBody.isEmpty()) {
                try {
                    return new JSONArray(responseBody);
                } catch (JSONException e) {
                    errorCollection.add(getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, serverName));
                    log.error("Bamboo Rest Request failed, could not parse response from " + serverName, e);
                }
            } else {
                errorCollection.add(getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, serverName));
                log.error("Bamboo Rest Request failed, no response received from " + serverName);
            }
        }

        return null;
    }

    private boolean isResponseValid(final String serverName, final BambooRestResponse response,
                                    final Collection<String> errorCollection, final boolean includeResponseBodyInError) {
        boolean validResponse = true;

        if (!response.getErrors().isEmpty()) {
            errorCollection.addAll(response.getErrors());
            validResponse = false;
        } else if (!response.isValidStatusCode()) {
            errorCollection.add(getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, serverName));

            log.error(String.format("Invalid return code received from %s. Response.statusCode: %d, statusMessage: %s, responseBody: %s",
                    serverName, response.getStatusCode(), response.getStatusMessage(),
                    includeResponseBodyInError ? response.getResponseBody() : "omitted"));

            validResponse = false;
        }

        return validResponse;
    }

    @Nonnull
    protected BambooRestResponse executePostRequest(@Nonnull final ApplicationLink applicationLink,
                                                    @Nonnull final String url, @Nonnull final Map<String, String> params, final int timeout)
            throws CredentialsRequiredException {
        final ApplicationLinkRequestFactory authenticatedRequestFactory = applicationLink.createAuthenticatedRequestFactory();
        return executePostRequest(authenticatedRequestFactory, url, params, timeout, applicationLink.getName());
    }

    @Nonnull
    protected BambooRestResponse executePostRequest(@Nonnull final ApplicationLinkRequestFactory authenticatedRequestFactory,
                                                    @Nonnull final String url, @Nonnull final Map<String, String> params, final int timeout, final String serverName)
            throws CredentialsRequiredException {
        final ApplicationLinkRequest request = authenticatedRequestFactory.createRequest(Request.MethodType.POST, url);

        for (final Map.Entry<String, String> param : params.entrySet()) {
            request.addRequestParameters(param.getKey(), param.getValue());
        }

        request.setHeader(X_ATLASSIAN_TOKEN, NO_CHECK);
        request.setHeader(CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED);

        return executeRequest(request, authenticatedRequestFactory, timeout, serverName);
    }

    @Nonnull
    protected BambooRestResponse executePutRequest(@Nonnull final ApplicationLink applicationLink,
                                                   @Nonnull final String url, @Nonnull final Map<String, String> params, final int timeout, final String serverName)
            throws CredentialsRequiredException {
        final ApplicationLinkRequestFactory authenticatedRequestFactory = applicationLink.createAuthenticatedRequestFactory();
        return executePutRequest(authenticatedRequestFactory, url, params, timeout, applicationLink.getName());
    }

    @Nonnull
    protected BambooRestResponse executePutRequest(@Nonnull final ApplicationLinkRequestFactory authenticatedRequestFactory,
                                                   @Nonnull final String url, @Nonnull final Map<String, String> params, final int timeout, final String serverName)
            throws CredentialsRequiredException {
        final UrlBuilder urlBuilder = new UrlBuilder(url);

        for (final Map.Entry<String, String> param : params.entrySet()) {
            urlBuilder.addParameter(param.getKey(), param.getValue());
        }

        final ApplicationLinkRequest request = authenticatedRequestFactory.createRequest(Request.MethodType.PUT, urlBuilder.asUrlString());
        return executeRequest(request, authenticatedRequestFactory, timeout, serverName);
    }

    @Nonnull
    protected BambooRestResponse executeGetRequest(@Nonnull ApplicationLink applicationLink, @Nonnull String url, final int timeout)
            throws CredentialsRequiredException {
        final ApplicationLinkRequestFactory authenticatedRequestFactory = applicationLink.createAuthenticatedRequestFactory();
        return executeGetRequest(authenticatedRequestFactory, url, timeout, applicationLink.getName());
    }

    @Nonnull
    protected BambooRestResponse executeGetRequest(@Nonnull final ApplicationLinkRequestFactory authenticatedRequestFactory,
                                                   @Nonnull final String url, final int timeout, final String serverName)
            throws CredentialsRequiredException {
        final ApplicationLinkRequest request = authenticatedRequestFactory.createRequest(Request.MethodType.GET, url);
        return executeRequest(request, authenticatedRequestFactory, timeout, serverName);
    }

    @Nonnull
    protected BambooRestResponse executeRequest(final ApplicationLinkRequest request,
                                                final ApplicationLinkRequestFactory authenticatedRequestFactory, int timeout, final String serverName)
            throws CredentialsRequiredException {
        request.setConnectionTimeout(timeout);
        request.setSoTimeout(timeout);
        return executeRequest(request, authenticatedRequestFactory, serverName);
    }

    @Nonnull
    protected BambooRestResponse executeRequest(final ApplicationLinkRequest request,
                                                final ApplicationLinkRequestFactory authenticatedRequestFactory, final String serverName)
            throws CredentialsRequiredException {
        final String bambooErrorMsg = getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, serverName);
        try {
            BambooRestResponse response = request.execute(new ApplicationLinkResponseHandler<BambooRestResponse>() {
                public BambooRestResponse handle(final Response response) throws ResponseException {
                    return new BambooRestResponse(response, bambooErrorMsg);
                }

                public BambooRestResponse credentialsRequired(Response response) throws ResponseException {
                    return new CredentialsRequiredResponse(response, bambooErrorMsg);
                }
            });

            if (response instanceof CredentialsRequiredResponse) {
                throw new CredentialsRequiredException(authenticatedRequestFactory, "Request failed. Credentials Required");
            } else {
                return response;
            }
        } catch (ResponseException e) {
            log.error("Could not connect to " + serverName, e);
            return new BambooRestResponse(bambooErrorMsg);
        }
    }

    private I18nHelper getI18nHelper() {
        return authenticationContext.getI18nHelper();
    }

    private static class CredentialsRequiredResponse extends BambooRestResponse {
        private CredentialsRequiredResponse(Response response, String failureErrorMsg) {
            super(response, failureErrorMsg);
        }
    }

    enum TriggerType {
        NEW,
        EXISTING
    }
}
