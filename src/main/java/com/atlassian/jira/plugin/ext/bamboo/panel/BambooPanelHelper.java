package com.atlassian.jira.plugin.ext.bamboo.panel;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooReleaseService;
import com.atlassian.jira.plugin.ext.bamboo.service.ReleaseErrorReportingService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import webwork.action.ActionContext;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static com.atlassian.jira.permission.GlobalPermissionKey.SYSTEM_ADMIN;
import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_BUILD_COMPLETED_STATE;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_BUILD_RESULT;
import static com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager.Filter.UNFILTERED;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.System.currentTimeMillis;
import static org.apache.commons.lang3.time.DateUtils.MILLIS_PER_DAY;

/**
 * Helper class for common things needed between the various panels.
 */
@Component
public class BambooPanelHelper {
    public static final String BAMBOO_PLUGIN_KEY = "com.atlassian.jira.plugin.ext.bamboo";
    public static final String SELECTED_SUB_TAB_KEY = "selectedSubTab";
    public static final String SUB_TAB_PLAN_STATUS = "planStatus";
    public static final String SUB_TAB_BUILD_BY_PLAN = "buildByPlan";
    public static final String SUB_TAB_BUILD_BY_DATE = "buildByDate";
    public static final List<String> SUB_TABS = ImmutableList.of(SUB_TAB_BUILD_BY_DATE, SUB_TAB_PLAN_STATUS);
    public static final List<String> ALL_SUB_TABS = ImmutableList.of(SUB_TAB_BUILD_BY_DATE, SUB_TAB_PLAN_STATUS, SUB_TAB_BUILD_BY_PLAN);

    @VisibleForTesting
    static final String BUILD_RESULT_KEY = "buildResultKey";

    @VisibleForTesting
    static final String BUILD_TRIGGERED_KEY = "buildTriggered";

    @VisibleForTesting
    static final String COMPLETED_STATE_KEY = "completedState";

    @VisibleForTesting
    static final String DAYS_REMAINING_KEY = "daysRemaining";

    @VisibleForTesting
    static final String HAS_APPLINKS_KEY = "hasApplinks";

    @VisibleForTesting
    static final String IS_PROJECT_ADMIN_KEY = "isProjectAdmin";

    @VisibleForTesting
    static final String OVERDUE_KEY = "overdue";

    @VisibleForTesting
    static final String RELEASE_ERRORS_KEY = "releaseErrors";

    private static final Pattern UNSAFE_CHARACTERS_REGEX = Pattern.compile("[\"'<>\\\\]");

    private final BambooApplicationLinkManager bambooApplicationLinkManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final BambooReleaseService bambooReleaseService;
    private final ReleaseErrorReportingService releaseErrorReportingService;
    private final GlobalPermissionManager globalPermissionManager;
    private final PermissionManager permissionManager;

    @Autowired
    public BambooPanelHelper(
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final GlobalPermissionManager globalPermissionManager,
            final BambooApplicationLinkManager bambooApplicationLinkManager,
            final BambooReleaseService bambooReleaseService,
            final ReleaseErrorReportingService releaseErrorReportingService) {
        this.bambooApplicationLinkManager = checkNotNull(bambooApplicationLinkManager);
        this.jiraAuthenticationContext = checkNotNull(jiraAuthenticationContext);
        this.globalPermissionManager = checkNotNull(globalPermissionManager);
        this.permissionManager = checkNotNull(permissionManager);
        this.bambooReleaseService = checkNotNull(bambooReleaseService);
        this.releaseErrorReportingService = checkNotNull(releaseErrorReportingService);
    }

    public void prepareVelocityContext(final Map<String, Object> velocityParams, final String bambooPluginModuleKey,
                                       final String baseLinkUrl, final String queryString, final List subTabs, final Project project) {
        String selectedSubTab;
        if (subTabs != null && !subTabs.isEmpty()) {
            velocityParams.put("availableTabs", subTabs);
            selectedSubTab = retrieveFromRequestOrSession(BambooPanelHelper.SELECTED_SUB_TAB_KEY, BambooPanelHelper.SUB_TAB_BUILD_BY_DATE);
            if (!ALL_SUB_TABS.contains(selectedSubTab)) {
                selectedSubTab = BambooPanelHelper.SUB_TAB_BUILD_BY_DATE;
            }
        } else {
            selectedSubTab = SUB_TAB_BUILD_BY_DATE;
        }
        velocityParams.put(BambooPanelHelper.SELECTED_SUB_TAB_KEY, selectedSubTab);

        if (BambooPanelHelper.SUB_TAB_BUILD_BY_DATE.equals(selectedSubTab)) {
            velocityParams.put("showRss", Boolean.TRUE);
        }

        prepareVelocityContext(velocityParams, bambooPluginModuleKey, baseLinkUrl, queryString, project);

    }

    public void prepareVelocityContext(final Map<String, Object> velocityParams, final String bambooPluginModuleKey,
                                       final String baseLinkUrl, final String queryString, final Project project) {
        ApplicationLink applicationLink = bambooApplicationLinkManager.getApplicationLink(project.getKey());

        velocityParams.put("moduleKey", bambooPluginModuleKey);
        velocityParams.put("querySection", queryString);
        velocityParams.put("baseLinkUrl", baseLinkUrl);
        velocityParams.put("baseResourceUrl", "/download/resources/" + BambooPanelHelper.BAMBOO_PLUGIN_KEY + ":" + bambooPluginModuleKey);

        if (applicationLink != null) {
            velocityParams.put("baseBambooUrl", applicationLink.getDisplayUrl().toASCIIString());
            velocityParams.put("baseBambooRestUrl", applicationLink.getRpcUrl() + "/rest/api/latest/");
        }

        velocityParams.put("baseRestUrl", "/rest/bamboo/1.0/");
        velocityParams.put("baseBambooRestProxyUrl", "/rest/bamboo/1.0/proxy/");

        sanitiseParams(velocityParams);

        //not sanitised on purpose
        if (applicationLink != null) {
            velocityParams.put("bambooServerName", applicationLink.getName());
        }

        velocityParams.put("isSystemAdmin",
                globalPermissionManager.hasPermission(SYSTEM_ADMIN, jiraAuthenticationContext.getLoggedInUser()));
    }

    public Map<String, Object> getReleaseButtonParams(ApplicationUser user, Version version, Project project) {
        final Map<String, Object> velocityParams = new HashMap<>();

        velocityParams.put(IS_PROJECT_ADMIN_KEY, isUserAuthorisedToReleaseProject(user, project));
        velocityParams.put(HAS_APPLINKS_KEY, isBambooAppLinked() && permissionManager.hasPermission(VIEW_DEV_TOOLS, project, user));

        addBuildParams(version, velocityParams, project.getKey());
        addReleaseErrors(version, velocityParams, project.getKey());
        addAnalyticsParams(version, velocityParams);

        return velocityParams;
    }

    private boolean isUserAuthorisedToReleaseProject(ApplicationUser user, Project project) {
        return bambooReleaseService.hasPermissionToRelease(user, project);
    }

    /**
     * Indicates whether there are any AppLinks to Bamboo servers.
     *
     * @return see above
     */
    public boolean isBambooAppLinked() {
        return bambooApplicationLinkManager.hasApplicationLinks(UNFILTERED);
    }

    private void addAnalyticsParams(
            @Nonnull final Version version, @Nonnull final Map<String, Object> velocityParams) {
        velocityParams.put(DAYS_REMAINING_KEY, getDaysRemaining(version));
        velocityParams.put(OVERDUE_KEY, isOverdue(version));
    }

    private void addBuildParams(final Version version, final Map<String, Object> velocityParams, final String projectKey) {
        final Map<String, String> buildParams = bambooReleaseService.getBuildData(projectKey, version.getId());
        if (buildParams != null) {
            velocityParams.put(BUILD_TRIGGERED_KEY, true);

            final String planResultKey = buildParams.get(PS_BUILD_RESULT);
            if (StringUtils.isNotBlank(planResultKey)) {
                velocityParams.put(BUILD_RESULT_KEY, planResultKey);
            }

            final String completedState = buildParams.get(PS_BUILD_COMPLETED_STATE);
            if (StringUtils.isNotBlank(completedState)) {
                velocityParams.put(COMPLETED_STATE_KEY, completedState);
            }
        }
    }

    private void addReleaseErrors(final Version version, final Map<String, Object> velocityParams, final String projectKey) {
        final List<String> releaseErrors = releaseErrorReportingService.getErrors(projectKey, version.getId());
        if (!releaseErrors.isEmpty()) {
            velocityParams.put(RELEASE_ERRORS_KEY, releaseErrors);
        }
    }

    private void sanitiseParams(final Map<String, Object> velocityParams) {
        for (Map.Entry<String, Object> entry : velocityParams.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof String) {
                entry.setValue(sanitiseString((String) value));
            }
        }
    }

    private String sanitiseString(final String string) {
        return UNSAFE_CHARACTERS_REGEX.matcher(string).replaceAll("");
    }

    private String retrieveFromRequestOrSession(final String requestKey, String defaultValue) {
        final String value = retrieveFromRequestOrSession(requestKey);
        return value != null ? value : defaultValue;
    }

    private String retrieveFromRequestOrSession(final String requestKey) {
        final String sessionKey = BambooPanelHelper.BAMBOO_PLUGIN_KEY + "." + requestKey;

        final String paramFromRequest = ParameterUtils.getStringParam(ActionContext.getParameters(), requestKey);
        final Map session = ActionContext.getSession();

        if (StringUtils.isNotBlank(paramFromRequest)) {
            // Sets param in session & return
            session.put(sessionKey, paramFromRequest);
            return paramFromRequest;
        } else {
            // Try to get it from the session
            return (String) session.get(sessionKey);
        }
    }

    @Nonnull
    private Object getDaysRemaining(@Nonnull final Version version) {
        if (version.getReleaseDate() == null) {
            return "null";
        }
        return (version.getReleaseDate().getTime() - currentTimeMillis()) / MILLIS_PER_DAY;
    }

    private boolean isOverdue(@Nonnull final Version version) {
        final Date releaseDate = version.getReleaseDate();
        return !version.isReleased() && releaseDate != null && releaseDate.before(new Date());
    }
}
