package com.atlassian.jira.plugin.ext.bamboo.service;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Map;

import static com.atlassian.jira.plugin.ext.bamboo.service.BambooRestServiceImpl.TRIGGER_CALL_TIMEOUT;
import static com.atlassian.sal.api.net.Request.MethodType.POST;
import static com.google.common.base.Preconditions.checkNotNull;

@Component
public class BambooServerAccessorImpl implements BambooServerAccessor {

    public static final String NO_CHECK = "no-check";
    public static final String X_ATLASSIAN_TOKEN = "X-Atlassian-Token";

    // Indicates the presence of a build result; will break when Bamboo changes HTML
    @VisibleForTesting
    static final String BUILD_RESULT_INDICATOR = "build_result";

    @VisibleForTesting
    static final String ISSUE_KEY_PARAM = "jiraIssueKey";

    // Presence indicates that the user needs to log in
    @VisibleForTesting
    static final String LOGIN_FORM_INDICATOR = "loginForm";

    @VisibleForTesting
    static final String NO_ASSOCIATED_BUILDS_I18N_KEY = "bamboo.tabpanel.no.associated.builds";

    @VisibleForTesting
    static final String NO_BAMBOO_APPLICATION_LINK_MESSAGE = "No Bamboo application link configured.";

    @VisibleForTesting
    static final String PROJECT_KEY_PARAM = "projectKey";

    @VisibleForTesting
    static final String[] DEFAULT_REQUEST_PARAMS = {"enableJavascript", "false", "maxBuilds", "25"};

    private final BambooApplicationLinkManager bambooApplicationLinkManager;
    private final BambooContentRewriter bambooContentRewriter;
    private final I18nHelper i18nHelper;
    private final ProjectManager projectManager;

    @Autowired
    public BambooServerAccessorImpl(
            @ComponentImport final I18nHelper i18nHelper,
            @ComponentImport final ProjectManager projectManager,
            final BambooApplicationLinkManager bambooApplicationLinkManager,
            final BambooContentRewriter bambooContentRewriter) {
        this.i18nHelper = checkNotNull(i18nHelper);
        this.bambooApplicationLinkManager = checkNotNull(bambooApplicationLinkManager);
        this.bambooContentRewriter = checkNotNull(bambooContentRewriter);
        this.projectManager = checkNotNull(projectManager);
    }

    public String getHtmlFromAction(
            final String bambooAction, final Project project, final Map<String, String> extraParams)
            throws IOException, ResponseException, CredentialsRequiredException {
        final LinkedHashSet<String> allProjectKeys = Sets.newLinkedHashSet();
        allProjectKeys.add(project.getKey()); // current key must be first
        allProjectKeys.addAll(projectManager.getAllProjectKeys(project.getId()));

        final ApplicationLink applicationLink = bambooApplicationLinkManager.getApplicationLink(project.getKey());
        final StringBuilder sb = new StringBuilder();
        for (final String key : allProjectKeys) {
            final Multimap<String, String> params = ArrayListMultimap.create();
            params.put(PROJECT_KEY_PARAM, key);

            if (extraParams != null) {
                params.putAll(Multimaps.forMap(extraParams));
            }

            final String result = getHtmlFromUrl(applicationLink, bambooAction, params);
            if (StringUtils.contains(result, BUILD_RESULT_INDICATOR)) {
                sb.append(result);
            }
        }
        if (sb.length() == 0) {
            sb.append("<p>");
            sb.append(i18nHelper.getText(NO_ASSOCIATED_BUILDS_I18N_KEY));
            sb.append("</p>");
        }
        return sb.toString();
    }

    public String getHtmlFromAction(final String bambooAction, final Project project, final Iterable<String> issueKeys,
                                    final Map<String, String> extraParams)
            throws IOException, ResponseException, CredentialsRequiredException {
        final Multimap<String, String> params = ArrayListMultimap.create();
        ApplicationLink applicationLink = bambooApplicationLinkManager.getApplicationLink(project.getKey());

        if (applicationLink == null) {
            return NO_BAMBOO_APPLICATION_LINK_MESSAGE;
        }

        for (String issueKey : issueKeys) {
            params.put(ISSUE_KEY_PARAM, issueKey);
        }

        if (extraParams != null) {
            params.putAll(Multimaps.forMap(extraParams));
        }

        return getHtmlFromUrl(applicationLink, bambooAction, params);
    }

    private String getHtmlFromUrl(
            final ApplicationLink applicationLink, final String bambooAction, final Multimap<String, String> params)
            throws IOException, CredentialsRequiredException, ResponseException {
        final ApplicationLinkRequest request =
                applicationLink.createAuthenticatedRequestFactory().createRequest(POST, bambooAction);

        request.addRequestParameters(DEFAULT_REQUEST_PARAMS);

        for (final String param : params.keySet()) {
            for (final String value : params.get(param)) {
                request.addRequestParameters(param, value);
            }
        }

        request.setSoTimeout(TRIGGER_CALL_TIMEOUT);
        request.setConnectionTimeout(TRIGGER_CALL_TIMEOUT);
        request.setHeader(X_ATLASSIAN_TOKEN, NO_CHECK);

        final String responseHtml = request.execute();

        //BDEV-3201: authentication failed and we got redirected to login page
        if (responseHtml.contains(LOGIN_FORM_INDICATOR)) {
            throw new ResponseException("Using applinks but got login form -> Authentication configuration problem.");
        }

        return bambooContentRewriter.rewriteHtml(responseHtml, applicationLink.getDisplayUrl().toASCIIString());
    }
}
