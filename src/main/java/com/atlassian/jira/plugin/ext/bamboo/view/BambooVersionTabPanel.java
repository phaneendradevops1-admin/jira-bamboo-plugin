package com.atlassian.jira.plugin.ext.bamboo.view;

import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooReleaseService;
import com.atlassian.jira.plugin.versionpanel.BrowseVersionContext;
import com.atlassian.jira.plugin.versionpanel.impl.GenericTabPanel;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;
import java.util.Map;

import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.BAMBOO_PLUGIN_KEY;
import static com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition.CONTEXT_KEY_PROJECT;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.singletonMap;

/**
 * Convenient superclass for this plugin's
 * {@link com.atlassian.jira.plugin.versionpanel.VersionTabPanel VersionTabPanels}.
 */
public abstract class BambooVersionTabPanel extends GenericTabPanel {
    @VisibleForTesting
    static final String SHOW_FUSION_FEEDBACK_BANNERS_KEY = "showReleaseReportBanners";

    protected final BambooPanelHelper bambooPanelHelper;
    protected final BambooReleaseService bambooReleaseService;
    private final PermissionManager permissionManager;
    private final String moduleKey;
    private final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition;

    protected BambooVersionTabPanel(
            @ComponentImport final FieldVisibilityManager fieldVisibilityManager,
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final SearchProvider searchProvider,
            final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition,
            final BambooPanelHelper bambooPanelHelper,
            final BambooReleaseService bambooReleaseService,
            final String moduleKey) {
        super(authenticationContext, searchProvider, fieldVisibilityManager);
        this.bambooPanelHelper = checkNotNull(bambooPanelHelper);
        this.bambooReleaseService = checkNotNull(bambooReleaseService);
        this.moduleKey = checkNotNull(moduleKey);
        this.permissionManager = checkNotNull(permissionManager);
        this.projectDevToolsIntegrationFeatureCondition = checkNotNull(projectDevToolsIntegrationFeatureCondition);
    }

    @Override
    public boolean showPanel(final BrowseVersionContext context) {
        final Map<String, Object> conditionContext = singletonMap(CONTEXT_KEY_PROJECT, context.getProject());
        return super.showPanel(context) && projectDevToolsIntegrationFeatureCondition.shouldDisplay(conditionContext)
                && userHasViewVersionControlPermissions(context);
    }

    private boolean userHasViewVersionControlPermissions(final BrowseVersionContext context) {
        return permissionManager.hasPermission(
                VIEW_DEV_TOOLS, context.getProject(), authenticationContext.getLoggedInUser());
    }

    @Override
    protected final Map<String, Object> createVelocityParams(final BrowseVersionContext context) {
        final Map<String, Object> velocityParams = super.createVelocityParams(context);
        final String baseLinkUrl = getBaseLinkUrl(context);
        final String queryString = "versionId=" + context.getVersion().getId();
        velocityParams.put(SHOW_FUSION_FEEDBACK_BANNERS_KEY, showFusionFeedbackBanner());
        addVelocityParams(context, velocityParams);
        prepareVelocityContext(velocityParams, baseLinkUrl, queryString, context.getProject());
        return velocityParams;
    }

    /**
     * Whether this panel should include the Fusion feedback banner. This
     * implementation returns <code>false</code>.
     *
     * @return the above flag
     */
    protected boolean showFusionFeedbackBanner() {
        return false;
    }

    /**
     * Subclasses should add any additional fields to the given Velocity view model.
     *
     * @param context        the current context
     * @param velocityParams the map to which to add any new entries
     */
    protected abstract void addVelocityParams(
            @Nonnull BrowseVersionContext context, @Nonnull Map<String, Object> velocityParams);

    /**
     * Subclasses should perform any final preparation of the Velocity context;
     * typically this involves calling the BambooPanelHelper.
     *
     * @param velocityParams the Velocity model
     * @param baseLinkUrl    the base link URL
     * @param queryString    the query string
     * @param project        the project whose version tab is being shown
     */
    protected abstract void prepareVelocityContext(
            Map<String, Object> velocityParams, String baseLinkUrl, String queryString, Project project);

    // -------------------- Helper methods for use by subclasses --------------------

    /**
     * Returns the base link URL for the given project and version
     *
     * @param context the current context
     * @return a URL relative to the context root of the Jira instance
     */
    protected final String getBaseLinkUrl(final BrowseVersionContext context) {
        return "/browse/" + context.getProject().getKey() +
                "/fixforversion/" + context.getVersion().getId() +
                "?selectedTab=" + BAMBOO_PLUGIN_KEY + ":" + moduleKey;
    }

    /**
     * This method changes the application state, which is not something a view should do.
     *
     * Logged as <a href="https://extranet.atlassian.com/jira/browse/BDEV-5994">BDEV-5994</a>.
     *
     * @param version the version
     */
    protected final void resetReleaseStateIfVersionWasUnreleased(final Version version) {
        bambooReleaseService.resetReleaseStateIfVersionWasUnreleased(version);
    }
}
