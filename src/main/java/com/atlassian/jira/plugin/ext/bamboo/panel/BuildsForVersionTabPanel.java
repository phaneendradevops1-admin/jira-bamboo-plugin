package com.atlassian.jira.plugin.ext.bamboo.panel;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooReleaseService;
import com.atlassian.jira.plugin.ext.bamboo.view.BambooVersionTabPanel;
import com.atlassian.jira.plugin.versionpanel.BrowseVersionContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;
import java.util.Map;

import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.SUB_TABS;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A {@link com.atlassian.jira.plugin.versionpanel.VersionTabPanel} that shows
 * build-related information for a specific version of a project.
 */
public class BuildsForVersionTabPanel extends BambooVersionTabPanel {
    // The unique key of this tab panel within this plugin
    @VisibleForTesting
    static final String MODULE_KEY = "bamboo-version-tabpanel";

    /*
        The dark feature flag that turns on the Fusion feedback banners (see FUSE-1514);
        we deliberately use the same flag as for the banners shown by the Jira Projects
        plugin, so that all these banners can be turned on or off as a set.
     */
    @VisibleForTesting
    static final String FEEDBACK_BANNER_DARK_FEATURE_FLAG = "jira.plugin.projects.version.tab.feedback.banner";

    private final FeatureManager featureManager;

    public BuildsForVersionTabPanel(
            final BambooPanelHelper bambooPanelHelper,
            final BambooReleaseService bambooReleaseService,
            @ComponentImport final FeatureManager featureManager,
            final FieldVisibilityManager fieldVisibilityManager,
            final JiraAuthenticationContext authenticationContext,
            final PermissionManager permissionManager,
            final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition,
            final SearchProvider searchProvider) {
        super(fieldVisibilityManager, authenticationContext, permissionManager, searchProvider,
                projectDevToolsIntegrationFeatureCondition, bambooPanelHelper, bambooReleaseService, MODULE_KEY);
        this.featureManager = checkNotNull(featureManager);
    }

    @Override
    public boolean showPanel(final BrowseVersionContext context) {
        return super.showPanel(context) && bambooPanelHelper.isBambooAppLinked();
    }

    @Override
    protected boolean showFusionFeedbackBanner() {
        return featureManager.isEnabled(FEEDBACK_BANNER_DARK_FEATURE_FLAG);
    }

    @Override
    protected void addVelocityParams(
            @Nonnull final BrowseVersionContext context, @Nonnull final Map<String, Object> velocityParams) {
        final Version version = context.getVersion();
        // This needs to happen before the release panel is shown
        resetReleaseStateIfVersionWasUnreleased(version);

        if (version.isReleased() && version.getReleaseDate() != null) {
            velocityParams.put("extraDescriptionKey", "released.");
        }
    }

    @Override
    protected void prepareVelocityContext(final Map<String, Object> velocityParams,
                                          final String baseLinkUrl, final String queryString, final Project project) {
        bambooPanelHelper.prepareVelocityContext(
                velocityParams, MODULE_KEY, baseLinkUrl, queryString, SUB_TABS, project);
    }
}
