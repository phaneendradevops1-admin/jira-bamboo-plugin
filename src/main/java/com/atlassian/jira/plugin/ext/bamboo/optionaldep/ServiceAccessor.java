package com.atlassian.jira.plugin.ext.bamboo.optionaldep;

import com.atlassian.util.concurrent.LazyReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

/**
 * Generic ServiceAccessor class. To reappropriate, simply implement the {@code getServiceFactoryClass()} method.
 *
 * @param <T> the type of the service
 */
public abstract class ServiceAccessor<T> {
    /**
     * Logger for OptionalServiceAccessor.
     */
    private static final Logger log = LoggerFactory.getLogger(ServiceAccessor.class);

    private final ApplicationContext applicationContext;

    private LazyReference<OptionalService<T>> serviceFactoryRef = new LazyReference<OptionalService<T>>() {
        @Override
        @SuppressWarnings("unchecked")
        protected OptionalService<T> create() throws Exception {
            try {
                Class<?> serviceFactoryClass = getServiceFactoryClass();
                if (serviceFactoryClass != null) {
                    // initializing the service factory only once
                    // this works for disabling/enabling of the plugin,
                    // but might not work when plugin is uninstalled and reinstalled
                    return (OptionalService<T>) applicationContext.getAutowireCapableBeanFactory()
                            .createBean(serviceFactoryClass, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR, false);
                }
            } catch (Exception e) {
                log.debug("Could not create service factory", e);
            }

            // fall back
            return null;
        }
    };

    protected ServiceAccessor(final ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * Returns a T, or {@code null} if the service is not available.
     *
     * @return a T, or {@code null}
     */
    public T getService() {
        OptionalService<T> serviceFactory = serviceFactoryRef.get();

        return serviceFactory != null ? serviceFactory.getService() : null;
    }

    /**
     * Returns the class of the service factory. Generally implemented as:
     * <pre>
     *     return MyFactory.class;
     * </pre>
     *
     * @return a Class object for a class that extends OptionalService
     */
    abstract protected Class<? extends OptionalService<T>> getServiceFactoryClass();
}