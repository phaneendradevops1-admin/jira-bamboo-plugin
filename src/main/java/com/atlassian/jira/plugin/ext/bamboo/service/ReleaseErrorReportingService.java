package com.atlassian.jira.plugin.ext.bamboo.service;

import javax.annotation.Nonnull;
import java.util.List;

public interface ReleaseErrorReportingService {
    /**
     * Records a release error message against the version.
     *
     * @param projectKey, the version is part of.
     * @param versionId,  the version to store the error against
     * @param error       string.
     */
    void recordError(@Nonnull String projectKey, long versionId, @Nonnull String error);

    /**
     * Records a release error message against the version.
     *
     * @param projectKey, the version is part of.
     * @param versionId,  the version to store the error against
     * @param errors      string.
     */
    void recordErrors(@Nonnull String projectKey, long versionId, @Nonnull List<String> errors);

    /**
     * Retrieves any release errors recorded against the version.
     *
     * @param projectKey the version belongs to
     * @param versionId  errors are recorded against
     * @return a list of any errors stored against the version.
     */
    @Nonnull
    List<String> getErrors(@Nonnull String projectKey, long versionId);

    /**
     * Removes all errors recorded against the version.
     *
     * @param projectKey the version belongs to
     * @param versionId  of the version to clear release errors from
     */
    void clearErrors(@Nonnull String projectKey, long versionId);
}
