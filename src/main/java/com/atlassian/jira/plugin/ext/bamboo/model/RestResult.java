package com.atlassian.jira.plugin.ext.bamboo.model;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

/**
 * Object that encapsulates data obtained from a REST request and any errors
 * that occurred when trying to make that request.
 *
 * @param <T> result object
 */
public class RestResult<T> {
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties

    private final ImmutableList<String> errors;
    private final T result;
    private final int statusCode;

    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors

    public RestResult(@Nullable final T result, @Nonnull final Collection<String> errors, int statusCode) {
        this.errors = ImmutableList.copyOf(errors);
        this.result = result;
        this.statusCode = statusCode;
    }

    // ----------------------------------------------------------------------------------------------- Interface Methods
    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators

    /**
     * @return result. There should be errors in {@link #getErrors()} if null
     */
    @Nullable
    public T getResult() {
        return result;
    }

    /**
     * @return list of errors
     */
    @Nonnull
    public ImmutableList<String> getErrors() {
        return errors;
    }

    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Returns a formatted error message suitable for {@link ErrorMessage}.
     *
     * @param defaultMessage the default message if there were no errors
     * @return error message
     */
    @Nonnull
    public String getErrorMessage(String defaultMessage) {
        if (!errors.isEmpty()) {
            return StringUtils.join(errors, "<br>");
        }
        return defaultMessage;
    }
}
