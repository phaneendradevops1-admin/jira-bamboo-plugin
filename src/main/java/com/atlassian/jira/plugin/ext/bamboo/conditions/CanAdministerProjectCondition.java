package com.atlassian.jira.plugin.ext.bamboo.conditions;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.Condition;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Checks that the current user is a project admin for the passed in Project, Version or Component.
 *
 * @since v5.0
 */
public class CanAdministerProjectCondition implements Condition {
    private final GlobalPermissionManager globalPermissionManager;
    private final JiraAuthenticationContext authContext;
    private final PermissionManager permissionManager;
    private final ProjectManager projectManager;

    public CanAdministerProjectCondition(
            @ComponentImport final GlobalPermissionManager globalPermissionManager,
            @ComponentImport final JiraAuthenticationContext authContext,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final ProjectManager projectManager) {
        this.authContext = checkNotNull(authContext);
        this.globalPermissionManager = checkNotNull(globalPermissionManager);
        this.permissionManager = checkNotNull(permissionManager);
        this.projectManager = checkNotNull(projectManager);
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        final ApplicationUser user = authContext.getLoggedInUser();
        final Project project = getProject(context);

        if (project == null) {
            return false;
        }

        final HttpServletRequest request = ExecutingHttpRequest.get();

        if (request == null) {
            return hasPermission(user, project);
        }

        String cacheKey = "atl.jira.permission.request.cache:" + ADMINISTER_PROJECTS.permissionKey() + ":" + user.getKey() + ":" + project.getKey();

        if (request.getAttribute(cacheKey) == null) {
            request.setAttribute(cacheKey, hasPermission(user, project));
        }

        return (Boolean) request.getAttribute(cacheKey);
    }

    private boolean hasPermission(ApplicationUser user, Project project) {
        try {
            return globalPermissionManager.hasPermission(ADMINISTER, user)
                    || permissionManager.hasPermission(ADMINISTER_PROJECTS, project, user);
        } catch (Exception e) {
            return false;
        }
    }

    private Project getProject(final Map<String, Object> context) {
        if (context.containsKey("project")) {
            return (Project) context.get("project");
        }

        if (context.containsKey("issue")) {
            return ((Issue) context.get("issue")).getProjectObject();
        }

        if (context.containsKey("helper")) {
            JiraHelper helper = (JiraHelper) context.get("helper");
            if (helper.getProject() != null) {
                return helper.getProject();
            }
        }

        if (context.containsKey("version")) {
            return ((Version) context.get("version")).getProject();
        }

        if (context.containsKey("component")) {
            final Long projectId = ((ProjectComponent) context.get("component")).getProjectId();
            return projectManager.getProjectObj(projectId);
        }

        return null;
    }
}
