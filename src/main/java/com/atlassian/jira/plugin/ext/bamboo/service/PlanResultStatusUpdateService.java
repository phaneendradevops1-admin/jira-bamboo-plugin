package com.atlassian.jira.plugin.ext.bamboo.service;

import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.jira.plugin.ext.bamboo.model.BuildState;
import com.atlassian.jira.plugin.ext.bamboo.model.LifeCycleState;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultKey;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultStatus;
import com.atlassian.jira.project.version.Version;

import javax.annotation.Nonnull;

/**
 * Async accessor for {@link BambooRestService#getPlanResultStatus(ApplicationLinkRequestFactory, PlanResultKey)}
 * designed for UI polling.
 */
public interface PlanResultStatusUpdateService {
    String INSTANCE_KEY = "INSTANCE";

    /**
     * Subscribe to status changes for the given project, version and planKey until the plan execution has stopped.
     *
     * @param version          the version
     * @param planResultKey    the plan result key
     * @param username         the username
     * @param finalizingAction the action to execute upon completion
     */
    void subscribe(@Nonnull Version version, @Nonnull PlanResultKey planResultKey, @Nonnull String username,
                   @Nonnull FinalizingAction finalizingAction);

    /**
     * Unsubscribe from status changes for the given {@link PlanResultKey}.
     *
     * @param planResultKey the plan result key
     */
    void unsubscribe(@Nonnull PlanResultKey planResultKey);

    /**
     * Schedule subscriptions for updates when they transition to
     * {@link BuildState#FAILED} or {@link BuildState#SUCCESS}.
     *
     * @return the number of updates scheduled by this call, i.e. not already scheduled
     */
    int scheduleUpdates();

    /**
     * An action to take upon a build completing.
     *
     * In Java 8, consider replacing with or extending a {@link java.util.function.Consumer} of {@link PlanResultStatus}.
     */
    interface FinalizingAction {
        /**
         * Executes this action.
         *
         * @param planStatus when the remote Plan reaches a finalized {@link LifeCycleState}
         */
        void execute(PlanResultStatus planStatus);
    }
}
