package com.atlassian.jira.plugin.ext.bamboo.service;

import javax.annotation.Nonnull;

/**
 * TODO: replace this with the service that is coming to a future SAL release
 */
public interface ImpersonationService {
    /**
     * Runs the given runnable as the given user.
     *
     * @param username the user as which to run the runnable
     * @param runnable the thing to run
     * @return runnable
     */
    @Nonnull
    Runnable runAsUser(@Nonnull final String username, @Nonnull Runnable runnable);
}
