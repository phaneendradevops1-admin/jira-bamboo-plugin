package com.atlassian.jira.plugin.ext.bamboo.upgrade.legacy.util;

import com.opensymphony.module.propertyset.PropertySet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Implementation of the LegacyBambooServerIdGenerator. This should only be used by the upgrade tasks.
 */
@Component
public class LegacyBambooServerIdGeneratorImpl implements LegacyBambooServerIdGenerator {
    public static final String CFG_ROOT = "bamboo.config";
    public static final String CFG_SERVER_NEXT_ID = CFG_ROOT + ".server.nextId";

    private final PropertySet propertySet;

    @Autowired
    public LegacyBambooServerIdGeneratorImpl(final LegacyBambooPropertyManager propertyManager) {
        this.propertySet = propertyManager.getPropertySet();
    }

    public int next() {
        int nextId = 1;
        if (propertySet.exists(CFG_SERVER_NEXT_ID)) {
            nextId = propertySet.getInt(CFG_SERVER_NEXT_ID);
        }
        propertySet.setInt(CFG_SERVER_NEXT_ID, nextId + 1);
        return nextId;
    }
}
