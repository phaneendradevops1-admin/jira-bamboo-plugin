package com.atlassian.jira.plugin.ext.bamboo.service;

import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultKey;
import org.apache.log4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlanExecutionResult {
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(PlanExecutionResult.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    // ------------------------------------------------------------------------------------------------- Type Properties
    private final PlanResultKey planResultKey;
    private final List<String> errors = new ArrayList<String>();

    // ---------------------------------------------------------------------------------------------------- Dependencies
    public PlanExecutionResult(@Nullable PlanResultKey planResultKey, @Nonnull List<String> errors) {
        this.planResultKey = planResultKey;
        this.errors.addAll(errors);
    }

    public PlanExecutionResult(@Nullable PlanResultKey planResultKey, @Nonnull String... errors) {
        this.planResultKey = planResultKey;
        Collections.addAll(this.errors, errors);
    }


    public PlanExecutionResult(@Nullable PlanResultKey planResultKey) {
        this.planResultKey = planResultKey;
    }

    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods
    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // ------------------------------------------------------------------------------------------------- Helper Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
    @Nullable
    public PlanResultKey getPlanResultKey() {
        return planResultKey;
    }

    @Nonnull
    public List<String> getErrors() {
        return errors;
    }
}
