package com.atlassian.jira.plugin.ext.bamboo.view;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.link.RemoteIssueLink;
import com.atlassian.jira.issue.link.RemoteIssueLinkBuilder;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.ext.bamboo.model.BuildState;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKeys;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultStatus;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanStatus;
import com.atlassian.jira.plugin.ext.bamboo.model.RestResult;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooRestService;
import com.atlassian.jira.plugin.viewissue.issuelink.DefaultIssueLinkRenderer;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.net.URI;
import java.util.Map;

import static com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition.CONTEXT_KEY_PROJECT;
import static com.google.common.base.Preconditions.checkNotNull;

public class BuildLinkRenderer extends DefaultIssueLinkRenderer {
    private static final Logger log = Logger.getLogger(BuildLinkRenderer.class);

    private static final String COMPONENT_KEY_I_18_N = "i18n";

    private final BambooApplicationLinkManager bambooApplicationLinkManager;
    private final BambooRestService bambooRestService;
    private final IssueManager issueManager;
    private final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition;

    public BuildLinkRenderer(
            @ComponentImport final IssueManager issueManager,
            final BambooApplicationLinkManager bambooApplicationLinkManager,
            final BambooRestService bambooRestService,
            final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition) {
        this.bambooApplicationLinkManager = checkNotNull(bambooApplicationLinkManager);
        this.bambooRestService = checkNotNull(bambooRestService);
        this.issueManager = checkNotNull(issueManager);
        this.projectDevToolsIntegrationFeatureCondition = checkNotNull(projectDevToolsIntegrationFeatureCondition);
    }

    @Override
    public Map<String, Object> getInitialContext(final RemoteIssueLink remoteIssueLink, final Map<String, Object> context) {
        final I18nHelper i18n = (I18nHelper) context.get(COMPONENT_KEY_I_18_N);
        final Map<String, Object> initialContext = Maps.newHashMap(super.getInitialContext(remoteIssueLink, context));
        fillContext(remoteIssueLink, i18n, BuildState.UNKNOWN, initialContext);
        return initialContext;
    }

    @Override
    public Map<String, Object> getFinalContext(RemoteIssueLink remoteIssueLink, final Map<String, Object> context) {
        final MutableIssue issue = issueManager.getIssueObject(remoteIssueLink.getIssueId());
        final String projectKey = issue.getProjectObject().getKey();

        final Map<String, Object> finalContext = Maps.newHashMap();

        ApplicationLink applicationLink = bambooApplicationLinkManager.getApplicationLink(projectKey);
        if (applicationLink != null) {
            final ApplicationLinkRequestFactory authenticatedRequestFactory = applicationLink.createAuthenticatedRequestFactory();

            try {
                boolean succeededFetchingStatus = false;
                BuildState buildState = null;
                RestResult restResult = null;
                if (remoteIssueLink.getGlobalId().contains("planResultKey")) {
                    final RestResult<PlanResultStatus> rr = bambooRestService.getPlanResultStatus(authenticatedRequestFactory, PlanKeys.getPlanResultKey(remoteIssueLink.getSummary()));
                    PlanResultStatus planResultStatus = rr.getResult();
                    if (planResultStatus != null) {
                        succeededFetchingStatus = true;
                        buildState = planResultStatus.getBuildState();
                    }
                    restResult = rr;
                } else if (remoteIssueLink.getGlobalId().contains("planKey")) {
                    RestResult<PlanStatus> rr = bambooRestService.getPlanStatus(authenticatedRequestFactory, PlanKeys.getPlanKey(remoteIssueLink.getSummary()));
                    PlanStatus planStatus = rr.getResult();
                    if (planStatus != null) {
                        succeededFetchingStatus = true;
                        buildState = planStatus.getLatestBuildState();
                    }
                    restResult = rr;
                } else {
                    log.warn("Bamboo doesn't know how to render this link: " + remoteIssueLink.getGlobalId() + ".  Perhaps the default rendering will be fine.");
                }

                if (restResult != null) {
                    if (succeededFetchingStatus && restResult.getErrors().isEmpty()) {
                        finalContext.put("iconClass", buildState == null ? "NeverExecuted" : buildState);
                        final I18nHelper i18n = (I18nHelper) context.get("i18n");
                        fillContext(remoteIssueLink, i18n, buildState, finalContext);
                    } else {
                        log.warn("Error when fetching Bamboo Plan status for key " + remoteIssueLink.getSummary() + " " + restResult.getErrors());
                        finalContext.put("noApplinkAuthConfigured", Boolean.TRUE);
                    }
                }
            } catch (final CredentialsRequiredException exception) {
                final URI authorisationURI = exception.getAuthorisationURI();
                if (authorisationURI == null) {
                    finalContext.put("noApplinkAuthConfigured", Boolean.TRUE);
                } else {
                    final String applicationName = StringUtils.defaultIfEmpty(applicationLink.getName(), "");
                    finalContext.put("authenticationRequired", Boolean.TRUE);
                    finalContext.put("authenticationUrl", exception.getAuthorisationURI());
                    finalContext.put("applicationName", applicationName);
                    finalContext.put("appLinkId", applicationLink.getId());
                    finalContext.put("applicationUrl", applicationLink.getDisplayUrl());
                    remoteIssueLink = new RemoteIssueLinkBuilder(remoteIssueLink).applicationName(applicationName).build();
                }
            }
        }
        finalContext.putAll(super.getInitialContext(remoteIssueLink, context));
        return finalContext;
    }

    private static void fillContext(final RemoteIssueLink remoteIssueLink, final I18nHelper i18n, final BuildState buildState, final Map<String, Object> initialContext) {
        final String i18nBuildState = i18n.getText("bamboo.build.state." + (buildState != null ? buildState.toString().toLowerCase() : "neverExecuted"));

        initialContext.put("iconTooltip", remoteIssueLink.getSummary() + " - " + i18nBuildState);
        initialContext.put("tooltip", "[" + remoteIssueLink.getApplicationName() + "] " + remoteIssueLink.getTitle());
    }

    @Override
    public boolean requiresAsyncLoading(final RemoteIssueLink remoteIssueLink) {
        return true;
    }

    @Override
    public boolean shouldDisplay(final RemoteIssueLink remoteIssueLink) {
        final Issue issue = issueManager.getIssueObject(remoteIssueLink.getIssueId());
        final Project project = issue.getProjectObject();
        Map<String, Object> context = ImmutableMap.<String, Object>of(CONTEXT_KEY_PROJECT, project);
        return super.shouldDisplay(remoteIssueLink)
                && projectDevToolsIntegrationFeatureCondition.shouldDisplay(context);
    }
}
