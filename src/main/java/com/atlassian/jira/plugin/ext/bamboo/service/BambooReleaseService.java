package com.atlassian.jira.plugin.ext.bamboo.service;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.ext.bamboo.model.BuildState;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultKey;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultStatus;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;

/**
 * Manages the Jira Release Bamboo integration
 */
public interface BambooReleaseService {
    /**
     * Executes the specified Bamboo plan with the Jira Release Trigger reason and Plan Trigger.
     *
     * @param applicationLink for the bamboo server to execute plan on
     * @param version         jira version the release build is for
     * @param settings        the configuration that is being used for the release
     * @return PlanExecutionResult containing any errors or planKey if successful.
     * @throws CredentialsRequiredException is authentication handshake is required
     */
    @Nonnull
    PlanExecutionResult triggerPlanForRelease(
            @Nonnull ApplicationLink applicationLink, @Nonnull Version version, @Nonnull Map<String, String> settings)
            throws CredentialsRequiredException;

    /**
     * Executes the specified bamboo result with the Jira Release Trigger reason and Plan Trigger.
     *
     * @param applicationLink for the bamboo server to execute plan on
     * @param version         jira version the release build is for
     * @param planResultKey   of the build to trigger
     * @param settings        the configuration that is being used for the release
     * @return PlanExecutionResult containing any errors or planKey if successful.
     * @throws CredentialsRequiredException is authentication handshake is required
     */
    @Nonnull
    PlanExecutionResult triggerExistingBuildForRelease(@Nonnull ApplicationLink applicationLink,
                                                       @Nonnull Version version, @Nonnull PlanResultKey planResultKey, @Nonnull Map<String, String> settings)
            throws CredentialsRequiredException;

    /**
     * Performs the Jira related components of the release.
     *
     * @param version       to be released
     * @param releaseConfig containing information on e.g. what to do with issues.
     */
    void releaseWithNoBuild(Version version, Map<String, String> releaseConfig);

    /**
     * Tries to release the provided {@link Version} when {@link PlanResultStatus#getBuildState()} is {@link BuildState#SUCCESS}.
     *
     * @param planStatus the plan status
     * @param version    the version
     * @return whether release was performed or not
     */
    boolean releaseIfRequired(@Nonnull PlanResultStatus planStatus, @Nonnull Version version);

    /**
     * Remove any release configuration data for the given version.
     *
     * @param projectKey of the project the version belongs to
     * @param versionId  of the version to remove associated configuration
     */
    void clearConfigData(@Nonnull String projectKey, long versionId);

    /**
     * Reset the state of the release management configuration and build data
     * if the version is unreleased and the build state is {@link BuildState#SUCCESS}.
     *
     * @param version to reset
     */
    void resetReleaseStateIfVersionWasUnreleased(@Nonnull Version version);

    /**
     * Retrieves the configuration data used for the release of that version.
     * Will only exist if release has been triggered and info may still be needed (i.e the release process is not yet complete).
     *
     * @param projectKey of the project the version belongs to
     * @param versionId  of the version to remove associated configuration
     * @return the configuration data used for the release of that version.
     */
    @Nullable
    Map<String, String> getConfigData(@Nonnull String projectKey, long versionId);

    /**
     * Get the default project settings for release management.
     *
     * @param projectKey of the project
     * @return config
     */
    @Nonnull
    Map<String, String> getDefaultSettings(@Nonnull String projectKey);

    /**
     * Records the start of a release build for this version.
     *
     * @param projectKey    of the project the version belongs to
     * @param versionId     of the version to remove associated configuration
     * @param planResultKey of the release build associated with this version
     */
    void registerReleaseStarted(@Nonnull String projectKey, long versionId, @Nonnull PlanResultKey planResultKey);

    /**
     * Gets the Build Data for the given version, includes a result key if a build, and whether that build has been
     * detected as complete. Only exists if a build has previously been triggered for this release.
     *
     * @param projectKey of the jira project the version belongs to
     * @param versionId  the version to get release info about
     * @return planResultKey
     */
    @Nullable
    Map<String, String> getBuildData(@Nonnull String projectKey, long versionId);

    /**
     * Checks whether the given user has permission to release the given project.
     *
     * @param user    the user whose permissions to check
     * @param project the project to which to check access
     * @return permissionToRelease
     * @since 7.3.5
     */
    boolean hasPermissionToRelease(@Nullable ApplicationUser user, @Nonnull Project project);

    /**
     * Checks whether the given user has permission to release the given project.
     *
     * @param user    the user whose permissions to check
     * @param project the project to which to check access
     * @return permissionToRelease
     * @deprecated Use {@link #hasPermissionToRelease(com.atlassian.jira.user.ApplicationUser, com.atlassian.jira.project.Project)} instead
     */
    @Deprecated
    boolean hasPermissionToRelease(@Nullable User user, @Nonnull Project project);

    /**
     * Filters a map for any key/values which have the variable prefix.  Variables are stored with "variable_" in front.
     *
     * @param toFilter         map to filter
     * @param prefixSubstitute the string to substitute bambooVeriable prefix with.
     * @return a map containing only bamboo variables.
     */
    @Nonnull
    Map<String, String> getBambooVariablesFromMap(@Nonnull Map<String, String> toFilter, @Nonnull String prefixSubstitute);
}
