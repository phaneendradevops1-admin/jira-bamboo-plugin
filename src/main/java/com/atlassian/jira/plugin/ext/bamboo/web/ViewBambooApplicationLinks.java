package com.atlassian.jira.plugin.ext.bamboo.web;

import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.atlassian.webresource.api.assembler.PageBuilderService;

@WebSudoRequired
public class ViewBambooApplicationLinks extends BambooWebActionSupport {
    public static final String JSPA_PATH = "ViewBambooApplicationLinks.jspa";

    public ViewBambooApplicationLinks(
            final BambooApplicationLinkManager applicationLinkManager,
            @ComponentImport final PageBuilderService jiraPageBuilderService) {
        super(jiraPageBuilderService, applicationLinkManager);
    }
}
