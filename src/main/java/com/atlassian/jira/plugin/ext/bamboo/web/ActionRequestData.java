package com.atlassian.jira.plugin.ext.bamboo.web;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates a method (usually a setter) that populates a Webwork action's field
 * with data read from an HTTP request.
 *
 * The motivation for this annotation is that you can tell IDEA not to warn you
 * that annotated methods are apparently unused (whereas in reality they are called
 * by Webwork upon receipt of an HTTP request).
 *
 * @see com.atlassian.jira.web.action.ActionViewData which indicates methods that populate the view-model
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.METHOD)
public @interface ActionRequestData {
    /**
     * An optional explanation of how this data is produced.
     *
     * @return a non-null explanation
     */
    String value() default "";
}
