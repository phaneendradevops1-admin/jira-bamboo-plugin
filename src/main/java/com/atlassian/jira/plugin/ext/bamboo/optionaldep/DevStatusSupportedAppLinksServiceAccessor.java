package com.atlassian.jira.plugin.ext.bamboo.optionaldep;

import com.atlassian.jira.plugin.devstatus.api.DevStatusSupportedApplicationLinkService;
import org.osgi.framework.BundleContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Accessor for optional dependency DevStatusSupportedApplicationLinkService
 */
@Component
public class DevStatusSupportedAppLinksServiceAccessor extends ServiceAccessor<DevStatusSupportedApplicationLinkService> {
    @Autowired
    public DevStatusSupportedAppLinksServiceAccessor(final ApplicationContext applicationContext) {
        super(applicationContext);
    }

    @Override
    protected Class<Factory> getServiceFactoryClass() {
        return Factory.class;
    }

    /**
     * This needs to be a concrete class so Spring can inject dependencies.
     */
    public static class Factory extends OptionalService<DevStatusSupportedApplicationLinkService> {
        public Factory(final BundleContext bundleContext) {
            super(bundleContext, DevStatusSupportedApplicationLinkService.class);
        }
    }
}
