## This Velocity template renders the release dialog.
## It reads the following variables from getters in ConfigureBambooRelease.java:

#* @vtlvariable name="applicationLinkName" type="java.lang.String" *#
#* @vtlvariable name="bambooLinked" type="boolean" *#
#* @vtlvariable name="buildType" type="java.lang.String" *#
#* @vtlvariable name="credentialsUrl" type="java.net.URI" *#
#* @vtlvariable name="currentReleaseDate" type="java.lang.String" *#
#* @vtlvariable name="errorMessages" type="java.util.Collection<java.lang.String>" *#
#* @vtlvariable name="errors" type="java.util.Map<java.lang.String, java.lang.String>" *#
#* @vtlvariable name="i18n" type="com.atlassian.sal.api.message.I18nResolver" *#
#* @vtlvariable name="moveUnfixedIssuesTo" type="java.lang.Long" *#
#* @vtlvariable name="openIssueCount" type="int" *#
#* @vtlvariable name="plansByProject" type="java.util.Map<com.atlassian.jira.plugin.ext.bamboo.model.BambooProject, java.util.List<com.atlassian.jira.plugin.ext.bamboo.model.BambooPlan>>" *#
#* @vtlvariable name="selectedPlanKey" type="java.lang.String" *#
#* @vtlvariable name="selectedStages" type="java.lang.String" *#
#* @vtlvariable name="textutils" type="com.opensymphony.util.TextUtils" *#
#* @vtlvariable name="unresolved" type="java.lang.String" *#
#* @vtlvariable name="variablesJson" type="java.lang.String" *#
#* @vtlvariable name="version" type="com.atlassian.jira.project.version.Version" *#
#* @vtlvariable name="versionId" type="long" *#
#* @vtlvariable name="versions" type="java.util.Collection<com.atlassian.jira.project.version.Version>" *#
#* @vtlvariable name="redirectUrlOnSuccess" type="java.lang.String" *#

################################################################################
## Directives                                                                 ##
################################################################################

#enable_html_escaping()

################################################################################
## Macros                                                                     ##
################################################################################

#macro(fielderror $fieldErrors $fieldName)
    #if ($fieldErrors.get($fieldName))
        <div class="error">$fieldErrors.get($fieldName)</div>
    #end
#end

#macro(messageBox $type $title $content)
    ## We call toString() on these values in case they are AnnotatedValue instances
    $soyRenderer.render('com.atlassian.auiplugin:aui-experimental-soy-templates','aui.message.message', {
        'type': ${type.toString()},
        'titleContent': ${title.toString()},
        'content': ${content.toString()}
    })
#end

#macro(renderErrors)
    #if ($errorMessages && $errorMessages.size() > 0)
        #if  ($errorMessages.size() == 1)
            #messageBox("error" $i18n.getText('bamboo.error.generic.title') $errorMessages.get(0))
        #else
            #messageBox("error" $i18n.getText('bamboo.error.generic.title') "
            <ul>
                #foreach ($error in $errorMessages)
                    <li>$textutils.htmlEncode($error)</li>
                #end
            </ul>
            ")
        #end
    #end
#end

## Shows the release dialog fields that are only relevant if Bamboo is connected
#macro(showBambooFields)
    <fieldset>
        <h3>Bamboo</h3>
        <fieldset class="group">
            <legend><span>Release</span></legend>
            <div class="radio">
                <input type="radio" class="radio" name="buildType" #if ($buildType && $buildType == "no-build")
                       checked="checked" #end id="no-build" value="no-build"/>
                <label for="no-build">with no build</label>
            </div>
            <div class="radio">
                <input type="radio" class="radio" name="buildType" #if (!$buildType || $buildType == "new-build")
                       checked="checked" #end id="new-build" value="new-build"/>
                <label for="new-build">with new build</label>
            </div>
            #fielderror ($errors "new-build")
            <div class="radio">
                <input type="radio" class="radio" name="buildType" #if ($buildType && $buildType == "existing-build")
                       checked="checked" #end id="existing-build" value="existing-build"/>
                <label for="existing-build">with existing build</label>
            </div>
            #fielderror ($errors "existing-build")
        </fieldset>
        <div class="field-group">
            <label for="bamboo-plan">Plan</label>
            <select class="select" id="bamboo-plan" name="selectedPlanKey">
                #foreach ( $project in $plansByProject.keySet() )
                    <optgroup label="$textutils.htmlEncode($project.name)">
                        #foreach ( $plan in $plansByProject.get($project) )
                            <option value="$textutils.htmlEncode($plan.key)"
                                #if ($selectedPlanKey && $selectedPlanKey == $plan.key)
                                    selected="selected"
                                    #if ($selectedStages) data-selected-stages="$textutils.htmlEncode($selectedStages)"#end
                                    #if ($variablesJson) data-selected-variables='$textutils.htmlEncode($variablesJson)'#end
                                #end
                                    >$textutils.htmlEncode($plan.shortName)</option>
                        #end
                    </optgroup>
                #end
            </select>
            #fielderror ($errors "selectedPlanKey")
        </div>
    </fieldset>
#end

## Shows the release dialog fields that are only relevant if there are unresolved issues in the version
#macro(showUnresolvedIssueFields)
    <fieldset>
        <h3>Remaining issues</h3>
        <fieldset class="group">
            <legend><span>$i18n.getText('bamboo.project.versions.release.unresolved.issues', $openIssueCount)</span></legend>
            <div class="radio">
                <input name="unresolved" id="unresolved-ignore" class="radio" #if (!$unresolved || $unresolved != "move") checked="checked" #end type="radio" value="ignore">
                #if ($versions && $versions.isEmpty())
                    #set($labelMessageCode = "bamboo.project.versions.release.ignore.issues.only.version")
                #else
                    #set($labelMessageCode = "bamboo.project.versions.release.ignore.issues")
                #end
                <label for="unresolved-ignore" id="unresolved-ignore-label">$i18n.getText($labelMessageCode)</label>
            </div>
            #if ($versions && $versions.size() > 0)
                <div class="radio">
                    <input name="unresolved" id="unresolved-move" class="radio" type="radio" value="move" #if ($unresolved && $unresolved == "move") checked="checked" #end>
                    <label for="unresolved-move" id="unresolved-move-label">$i18n.getText('bamboo.project.versions.release.move.issues.to.version', $openIssueCount): </label>
                    <select name="moveUnfixedIssuesTo" id="moveUnfixedIssuesTo" class="select">
                        #foreach ($version in $versions)
                            <option value="$version.id" #if ($moveUnfixedIssuesTo && $moveUnfixedIssuesTo == $version.id)selected="selected" #end>$version.name</option>
                        #end
                    </select>
                </div>
            #end
            #fielderror ($errors "moveUnfixedIssuesTo")
            #fielderror ($errors "unresolved")
        </fieldset>
    </fieldset>
#end

## Shows the controls for entering/selecting the release date
#macro(showReleaseDateFields)
    ## currentCalendar and currentMillis are provided by Jira
    #set ($firstDay = $currentCalendar.firstDayOfWeek - 1)

    <div class="field-group" id="release-form-date-picker">
        ## These hidden fields are looked up from common.js by their title attribute
        <input type="hidden" title="dateFormat" value="$dateFormat" />
        <input type="hidden" title="currentReleaseDate" value="$currentReleaseDate" />
        <input type="hidden" title="formattedReleaseDate" value="$formattedReleaseDate" />
        <input type="hidden" title="currentMillis" value="$currentMillis" />
        <input type="hidden" title="firstDay" value="$firstDay" />
        <input type="hidden" title="useISO8601" value="$currentCalendar.useISO8601"/>

        <label for="release-form-release-date-field">$i18n.getText('version.releasedate')</label>
        <input id="release-form-release-date-field"
               name="userReleaseDate" type="text" class="text medium-field" />&nbsp;
        <a href="#" id="release-form-release-date-trigger" title="$i18n.getText('date.picker.select.date')">
            <span class="aui-icon icon-date">$i18n.getText('date.picker.select.date')</span>
        </a>
        #if ($errors.releaseDate)
            <div class="error release-form-error" id="release-form-release-date-error">$errors.releaseDate</div>
        #end
    </div>
#end

#macro(promptForAuthorisation)
<div class="form-body">
    #messageBox("warning" $i18n.getText('bamboo.error.authentication.required.title')
        $i18n.getText('bamboo.error.authentication.required', $credentialsUrl.toString(), $applicationLinkName)
    )
</div>
#end

#macro(showButtonBar)
    <div class="buttons-container form-footer">
        <div class="buttons">
            <input id="release" class="submit button" type="submit" value="Release"/>
            <a class="cancel">Cancel</a>
        </div>
    </div>
#end

#macro(insertAnalyticsScript)
    <script type="text/javascript">
        (function () {
            AJS.$('#release').click(function() {
                JBAM.publishAnalyticsForReleaseDialogClicked($versionId, AJS.$('input[name=buildType]:checked').val());
            });
        })();
    </script>
#end

################################################################################
## The release dialog itself (the main rendering code)                        ##
################################################################################

<h2>Release $version.name</h2>

    ## Show the release form
    <form id="release-form" class="aui long-label" action="${req.contextPath}/secure/SubmitBambooRelease.jspa" method="post">
        <div class="form-body">

            #if ($credentialsUrl)
                #promptForAuthorisation()
                #renderErrors()

            #elseif ($errorMessages && $errorMessages.size() > 0)
                #renderErrors()

            #elseif ($plansByProject.isEmpty())
                #messageBox("info" $i18n.getText('bamboo.no.active.plans.title') $i18n.getText('bamboo.no.active.plans'))
            #elseif ($bambooLinked)
                #showBambooFields()
            #end

            ##
            ## Renderer for release version form
            ##
            ## Requires:
            ## @param openIssueCount = number of issues unresolved
            ## @param versions = available unresolved versions
            ## @param version = the version object being released
            ##
            ## Produces:
            ## unresolved = move|ignore
            ## moveUnfixedIssuesTo = version id.

            #if ($openIssueCount && $openIssueCount > 0)
                #showUnresolvedIssueFields()
            #else
                <input type="hidden" name="unresolved" value="ignore"/>
            #end

            #showReleaseDateFields()
        </div>
        <input type="hidden" name="versionId" value="$versionId" />
        <input type="hidden" name="redirectUrlOnSuccess" value="$redirectUrlOnSuccess" />
        #showButtonBar()
        #insertAnalyticsScript()
    </form>

