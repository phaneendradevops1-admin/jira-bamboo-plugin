#* @vtlvariable name="buildResultKey" type="java.lang.String" *#
#* @vtlvariable name="buildTriggered" type="java.lang.String" *#
#* @vtlvariable name="completedState" type="com.atlassian.jira.plugin.ext.bamboo.model.BuildState" *#
#* @vtlvariable name="version" type="com.atlassian.jira.project.version.Version" *#
#* @vtlvariable name="project" type="com.atlassian.jira.project.Project" *#
#* @vtlvariable name="i18n" type="com.atlassian.sal.api.message.I18nResolver" *#
#* @vtlvariable name="soyRenderer" type="com.atlassian.soy.renderer.SoyTemplateRenderer" *#
#* @vtlvariable name="overdue" type="java.lang.Boolean" *#

#enable_html_escaping()

#*
 * Displays the release button.
 *
 * @param isProjectAdmin
 * @param req the HTTP request
 * @param version
 * @param retry whether this is a retry, or regular release button
 * @param label the button label
 *#
#macro(releaseButton $isProjectAdmin $req $version $retry $label)
    #if ($isProjectAdmin)
        <a id="runRelease" class="aui-button #if (!$retry) aui-button-primary #end"
           href="${req.contextPath}/secure/ConfigureBambooRelease!input.jspa?versionId=$version.id">$label</a>
    #end
#end

#if ($showReleaseReportBanners)
    #parse("/templates/plugins/bamboo/feedbackBanner.vm")
#end

#if ($buildResultKey)
    <div>
        <div>
            <ul class="item-details">
                <li id="build-details"></li>
            </ul>
        </div>
    </div>
#end
<div id="release-status" class="#if (!$version.released && !$buildResultKey)version-unreleased#end">
    #if ($buildResultKey && $completedState)
        #if (!$version.released)
            #releaseButton($isProjectAdmin, $req, $version, true, "Retry release")
        #end
    #elseif (!$version.released && !$buildTriggered)
        #releaseButton($isProjectAdmin, $req, $version, false, "Release")
    #end
</div>
<script type="text/javascript">

        require(['aui/flag'], function(flag) {
            #if ($releaseErrors && $isProjectAdmin)
                #foreach ($error in $releaseErrors)
                    flag({
                        type: 'error',
                        title: 'Failure',
                        close: 'manual',
                        body: "$error"
                    });
                #end

                document.addEventListener('aui-flag-close', function(e) {
                    JBAM.clearReleaseErrors();
                });
            #end

            #if ($hasApplinks)
                (function (JBAM) {
                    JBAM.jiraProjectKey = "$project.key";
                    JBAM.jiraVersionId = "$version.id";
                    JBAM.baseURL = "$req.contextPath$baseLinkUrl";
                    JBAM.restURL = "$req.contextPath$baseRestUrl";
                    JBAM.bambooBaseURL = "$baseBambooUrl";
                }(window.JBAM = (window.JBAM || {})));

                #if ($buildResultKey)
                    JBAM.ShowBuildReleaseDetails("$buildResultKey");
                #end
            #end

            AJS.$('#runRelease').click(function() {
                JBAM.publishAnalyticsForReleaseClicked($version.id, $overdue, $daysRemaining);
            });
        });
</script>