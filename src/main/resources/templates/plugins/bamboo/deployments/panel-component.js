define('jira/bamboo-plugin/component/deployments/panel', [
    'jira/skate'
], function (skate) {
    return skate('jbp-deployments-panel', {
        type: skate.type.CLASSNAME,

        attached: function (elem) {
            new JBAM_DEPLOYMENT.RelatedDeploymentProjects.View({
                el: '.jbp-deployments-panel',
                fetchUrl: elem.getAttribute('data-fetch-url'),
                oAuthDanceReturnUrl: elem.getAttribute('data-oauth-dance-return-url'),
                issueId: elem.getAttribute('data-issue-id')
            });
        }
    });
});