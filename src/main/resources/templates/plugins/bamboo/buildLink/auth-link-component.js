define('jira/bamboo-plugin/component/issue-link/auth-link', [
    'jira/skate',
    'jquery'
], function (skate,
             jQuery) {
    return skate('jbp-issue-link-auth', {
        type: skate.type.CLASSNAME,

        attached: function (elem) {
            ApplinksUtils.createAuthRequestInline(null, {
                id: elem.getAttribute('data-app-link-id'),
                appName: elem.getAttribute('data-application-name'),
                appUri: elem.getAttribute('data-application-url'),
                authUri: elem.getAttribute('data-authentication-url')
            }).each(function (index, singleElement) {
                jQuery(elem).append(singleElement);
            });
        }
    });
});