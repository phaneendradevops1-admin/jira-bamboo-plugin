require(['jquery', 'underscore', 'jira/bamboo-plugin/brace'], function ($, _, Brace) {
    window.JBAM_DEPLOYMENT = (window.JBAM_DEPLOYMENT || {});

    /**Brace model starts here */
    JBAM_DEPLOYMENT.Model = {};

    JBAM_DEPLOYMENT.Model.DeploymentProject = Brace.Model.extend({
        namedAttributes: {
            name: 'string',
            bambooId: 'number',
            url: 'string',
            environmentCount: 'number',
            upToDateEnvironmentCount: 'number',
            applinkId: 'string'
        }
    });

    JBAM_DEPLOYMENT.Model.Version = Brace.Model.extend({
        namedAttributes: {
            name: 'string',
            id: 'number',
            url: 'string'
        }
    });

    JBAM_DEPLOYMENT.Model.Environment = Brace.Model.extend({
        namedAttributes: {
            name: 'string',
            id: 'number',
            issueStatus: 'string',
            statusOk: 'boolean',
            url: 'string',
            version: JBAM_DEPLOYMENT.Model.Version
        }
    });

    JBAM_DEPLOYMENT.Model.IssueDeploymentStatus = Brace.Model.extend({
        fetchUrl: '',
        namedAttributes: {
            deploymentProject: JBAM_DEPLOYMENT.Model.DeploymentProject,
            versions: Brace.Collection.extend({
                model: JBAM_DEPLOYMENT.Model.Version
            }),
            environments: Brace.Collection.extend({
                model: JBAM_DEPLOYMENT.Model.Environment
            })
        },
        url: function () {
            return this.fetchUrl;
        },
        initialize: function (bootstrap, options) {
            this.fetchUrl = options.fetchUrl;
        }
    });

    JBAM_DEPLOYMENT.Model.ErrorMessage = Brace.Model.extend({
        namedAttributes: {
            message: 'string',
            messageBody: 'string'
        }
    });

    JBAM_DEPLOYMENT.Model.OauthCallback = Brace.Model.extend({
        namedAttributes: {
            oauthCallback: 'string',
            serverName: 'string'
        }
    });

    JBAM_DEPLOYMENT.Model.RelatedDeploymentProjects = Brace.Model.extend({
        fetchUrl: '',
        namedAttributes: {
            deploymentProjects: Brace.Collection.extend({
                model: JBAM_DEPLOYMENT.Model.DeploymentProject
            }),
            errorMessages: Brace.Collection.extend({
                model: JBAM_DEPLOYMENT.Model.ErrorMessage
            }),
            oauthCallbacks: Brace.Collection.extend({
                model: JBAM_DEPLOYMENT.Model.OauthCallback
            })
        },
        url: function () {
            return this.fetchUrl;
        },
        initialize: function (bootstrap, options) {
            this.fetchUrl = options.fetchUrl;
        }
    });

    /**Brace model ends here */
    JBAM_DEPLOYMENT.IssueDeploymentStatus = {};

    JBAM_DEPLOYMENT.IssueDeploymentStatus.View = Brace.View.extend({
        oAuthDanceReturnUrl: '',
        initialize: function (options) {
            this.options.bootstrap = options.bootstrap || [];
            this.oAuthDanceReturnUrl = options.oAuthDanceReturnUrl || '';
            this.model = new JBAM_DEPLOYMENT.Model.IssueDeploymentStatus(this.options.bootstrap, {fetchUrl: options.fetchUrl});
            this.update();
        },
        render: function () {
            this.$el.html(bamboo.deployments.issueDetailsDialogContent({data: this.model.toJSON()}));
            return this;
        },
        update: function () {
            return this.model.fetch({cache: false})
                .done(_.bind(this.render, this))
                .fail(_.bind(this.removeSpinner, this))
                .fail(_.bind(this.clearError, this))
                .fail(_.bind(this.displayError, this))
        },
        displayError: function (jqXHR, textStatus, errorThrown) {
            HandleErrorResponse(jqXHR, textStatus, errorThrown, this.$el);
        },
        clearError: function () {
            this.$el.children('.aui-message').remove();
        },
        removeSpinner: function () {
            this.$el.children('.bamboo-icon-loading').remove();
        }
    });

    JBAM_DEPLOYMENT.RelatedDeploymentProjects = {};

    JBAM_DEPLOYMENT.RelatedDeploymentProjects.PopupDialog = JBAM_DEPLOYMENT.RelatedDeploymentProjects.PopupDialog
        || new JIRA.FormDialog({
            trigger: ".bamboo-deployments-details-link",
            width: 640,
            autoClose: true
        });

    JBAM_DEPLOYMENT.RelatedDeploymentProjects.View = Brace.View.extend({
        initialize: function (options) {
            this.options.bootstrap = options.bootstrap || [];
            this.oAuthDanceReturnUrl = options.oAuthDanceReturnUrl || '';
            this.issueId = options.issueId || '';
            this.selector = options.el;
            this.model = new JBAM_DEPLOYMENT.Model.RelatedDeploymentProjects(this.options.bootstrap, {fetchUrl: options.fetchUrl});
            this.update();
        },
        render: function () {
            $(this.selector).html(bamboo.deployments.issuePanelContent({
                data: this.model.toJSON(),
                issueId: this.issueId,
                oAuthDanceReturnUrl: encodeURIComponent(this.oAuthDanceReturnUrl)
            }));
            return this.showPanelIfNotEmpty();
        },
        showPanelIfNotEmpty: function () {
            if (!$(this.selector).is(":empty")) {
                $("#bamboo-deployment-projects-right").show();
            } else {
                $("#bamboo-deployment-projects-right").hide();
            }
            return this;
        },
        update: function () {
            return this.model.fetch({cache: false})
                .done(_.bind(this.render, this))
                .fail(_.bind(this.displayError, this));
        },
        displayError: function (jqXHR, textStatus, errorThrown) {
            $(this.selector).children('.aui-message').remove();
            HandleErrorResponse(jqXHR, textStatus, errorThrown, $(this.selector));
        }
    });

    function HandleErrorResponse(jqXHR, textStatus, errorThrown, $el) {
        var json, title, xhrResponse;
        try {
            xhrResponse = jqXHR.responseText || jqXHR.response;
            if (xhrResponse) {
                json = $.parseJSON(xhrResponse);
                $el.append(bamboo.components.errorResponseMessage({
                    errorMessage: json,
                    oAuthDanceReturnUrl: encodeURIComponent(this.oAuthDanceReturnUrl)
                }));
            } else {
                title = "Unknown error: no response from server";
            }
        }
        catch (e) {
            title = "An error occurred while trying to retrieve the deployment project information.";
        }
        if (title) {
            AJS.messages.warning($el, {
                title: title,
                closeable: false
            });
        }
        $("#bamboo-deployment-projects-right").show();
    }
});
