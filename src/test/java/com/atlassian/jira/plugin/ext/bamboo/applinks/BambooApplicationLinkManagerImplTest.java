package com.atlassian.jira.plugin.ext.bamboo.applinks;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.application.bamboo.BambooApplicationType;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.plugin.devstatus.api.DevStatusSupportedApplicationLinkService;
import com.atlassian.jira.plugin.ext.bamboo.optionaldep.DevStatusSupportedAppLinksServiceAccessor;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.navlink.consumer.menu.services.RemoteApplications;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import static com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManagerImpl.JBAM_ASSOCIATIONS;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BambooApplicationLinkManagerImplTest {
    private static final String PROJECT1 = "ONE";
    private static final String PROJECT2 = "TWO";

    private BambooApplicationLinkManager applinkMgr;
    private List<String> associations;
    private List<String> app2associations;

    @Mock
    private ApplicationLink applink;
    @Mock
    private ApplicationLink applink2;
    @Mock
    private ApplicationLinkService applinkService;
    @Mock
    private BuildUtilsInfo jiraBuildInfo;
    @Mock
    private DevStatusSupportedApplicationLinkService devStatusSupportedApplicationLinkService;
    @Mock
    private DevStatusSupportedAppLinksServiceAccessor devStatusSupportedAppLinksServiceAccessor;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private PluginAccessor jiraPluginManager;
    @Mock
    private RemoteApplications remoteApplications;

    @Before
    public void setup() throws Exception {
        associations = new ArrayList<>();
        app2associations = new ArrayList<>();

        when(applink.getId()).thenReturn(new ApplicationId(UUID.randomUUID().toString()));
        when(applink.putProperty(eq(JBAM_ASSOCIATIONS), anyObject())).thenAnswer(invocation -> {
            associations = (List<String>) invocation.getArguments()[1];
            return null;
        });
        when(applink.getProperty(JBAM_ASSOCIATIONS)).thenAnswer(invocationOnMock -> getAssociations());
        when(applink.removeProperty(JBAM_ASSOCIATIONS)).thenAnswer(invocationOnMock -> {
            associations = new ArrayList<>();
            return null;
        });

        when(applink2.putProperty(eq(JBAM_ASSOCIATIONS), anyObject())).thenAnswer(invocation -> {
            app2associations = (List<String>) invocation.getArguments()[1];
            return null;
        });
        when(applink2.getProperty(JBAM_ASSOCIATIONS)).thenAnswer(invocationOnMock -> app2associations);
        when(applink2.getId()).thenReturn(new ApplicationId(UUID.randomUUID().toString()));

        when(applinkService.getApplicationLink(applink.getId())).thenReturn(applink);
        when(applinkService.getApplicationLink(applink2.getId())).thenReturn(applink2);
        when(applinkService.getPrimaryApplicationLink(BambooApplicationType.class)).thenReturn(applink);
        when(applinkService.getApplicationLinks(BambooApplicationType.class)).thenReturn(Lists.newArrayList(applink, applink2));

        applinkMgr = new BambooApplicationLinkManagerImpl(applinkService, jiraBuildInfo, eventPublisher, featureManager,
                remoteApplications, jiraPluginManager, devStatusSupportedAppLinksServiceAccessor) {
            @Override
            protected boolean shouldFilterBambooApplinks() {
                return true;
            }
        };
    }

    @Test
    public void projectIsAssociatedWithPrimaryApplinkByDefault() {
        assertThat(applinkMgr.getApplicationLink(PROJECT1), is(equalTo(applink)));
    }

    @Test
    public void projectIsAssociatedWithCorrectApplink() {
        applinkMgr.associate(PROJECT1, applink2.getId());
        assertThat(applinkMgr.getApplicationLink(PROJECT1), is(equalTo(applink2)));
    }

    @Test
    public void associateFirstProject() {
        applinkMgr.associate(PROJECT1, applink.getId());
        List<String> associations = (List<String>) applink.getProperty(JBAM_ASSOCIATIONS);
        assertThat(associations.size(), is(equalTo(1)));
        assertThat(associations, hasItems(PROJECT1));
    }

    @Test
    public void associateSecondProject() {
        applinkMgr.associate(PROJECT1, applink.getId());
        applinkMgr.associate(PROJECT2, applink.getId());
        List<String> associations = (List<String>) applink.getProperty(JBAM_ASSOCIATIONS);
        assertThat(associations.size(), is(equalTo(2)));
        assertThat(associations, hasItems(PROJECT1, PROJECT2));
    }

    @Test
    public void associateSecondProjectAfterUnassociatingFirstProject() {
        applinkMgr.associate(PROJECT1, applink.getId());
        applinkMgr.unassociateAll(applink.getId());
        applinkMgr.associate(PROJECT2, applink.getId());
        List<String> associations = (List<String>) applink.getProperty(JBAM_ASSOCIATIONS);
        assertThat(associations.size(), is(equalTo(1)));
        assertThat(associations, hasItems(PROJECT2));
    }

    @Test
    public void testGetApplicationLinksForBuildSummaryFilter() {
        when(devStatusSupportedAppLinksServiceAccessor.getService()).thenReturn(devStatusSupportedApplicationLinkService);
        when(devStatusSupportedApplicationLinkService.hasApplicationLink2LOSupport(applink)).thenReturn(true);
        when(devStatusSupportedApplicationLinkService.hasApplicationLink2LOSupport(applink2)).thenReturn(false);

        List<ApplicationLink> appLinks = Lists.newArrayList(applinkMgr.getApplicationLinks(BambooApplicationLinkManager.Filter.SKIP_LINKS_WITH_BUILD_SUMMARY_CAPABILITY_IF_FUSION_ENABLED));
        assertThat(appLinks.size(), is(equalTo(1)));
        assertThat(appLinks, hasItem(applink2));
    }

    @Test
    public void testGetApplicationLinksForDeploymentSummaryFilter() {
        when(devStatusSupportedAppLinksServiceAccessor.getService()).thenReturn(devStatusSupportedApplicationLinkService);
        when(devStatusSupportedApplicationLinkService.hasApplicationLink2LOSupport(applink)).thenReturn(true);
        when(devStatusSupportedApplicationLinkService.hasApplicationLink2LOSupport(applink2)).thenReturn(false);
        when(remoteApplications.capableOf(BambooApplicationLinkManagerImpl.DEV_STATUS_DETAIL_DEPLOYMENT_ENVIRONMENT_CAPABILITY)).thenReturn(new HashSet());

        List<ApplicationLink> appLinks = Lists.newArrayList(applinkMgr.getApplicationLinks(BambooApplicationLinkManager.Filter.SKIP_LINKS_WITH_DEPLOYMENT_SUMMARY_CAPABILITY_IF_FUSION_ENABLED));
        assertThat(appLinks.size(), is(equalTo(2)));
        assertThat(appLinks, hasItem(applink2));
    }

    private List<String> getAssociations() {
        return associations;
    }
}
