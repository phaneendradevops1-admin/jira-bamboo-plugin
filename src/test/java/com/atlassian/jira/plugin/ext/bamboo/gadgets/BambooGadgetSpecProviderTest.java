package com.atlassian.jira.plugin.ext.bamboo.gadgets;

import com.atlassian.applinks.api.ApplicationId;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class BambooGadgetSpecProviderTest {

    private BambooGadgetSpecProvider objectUnderTest;

    @Before
    public void setUp() {
        objectUnderTest = new BambooGadgetSpecProvider();
    }

    @Test
    public void entriesShouldReturnCorrectContents() {
        // Set up
        final ApplicationId appId1 = mock(ApplicationId.class);
        final ApplicationId appId2 = mock(ApplicationId.class);
        final URI gadgetUri1 = URI.create("http://www.example.com/1");
        final URI gadgetUri2 = URI.create("http://www.example.com/2");
        final URI gadgetUri3 = URI.create("http://www.example.com/3");
        objectUnderTest.addBambooGadget(appId1, gadgetUri1);
        objectUnderTest.addBambooGadget(appId2, gadgetUri2);
        objectUnderTest.addBambooGadget(appId2, gadgetUri3);

        // Invoke
        final Iterable<URI> entries = objectUnderTest.entries();

        // Check
        assertThat(entries, contains(gadgetUri1, gadgetUri2, gadgetUri3));
    }
}