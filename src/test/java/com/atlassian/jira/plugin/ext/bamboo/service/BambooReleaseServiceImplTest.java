package com.atlassian.jira.plugin.ext.bamboo.service;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.propertyset.JiraPropertySetFactory;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.DateFieldFormat;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.Map;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.permission.ProjectPermissions.ADMINISTER_PROJECTS;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_CONFIG_RELEASE_DATE;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.VARIABLE_PARAM_PREFIX;
import static java.util.Collections.singletonMap;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BambooReleaseServiceImplTest {
    @Mock
    private Project project;

    @Mock
    private BambooRestService bambooRestService;
    @Mock
    private DateFieldFormat dateFieldFormat;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private IssueManager issueManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private JiraPropertySetFactory jiraPropertySetFactory;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private PlanResultStatusUpdateService planStatusUpdateService;
    @Mock
    private PluginSettingsFactory pluginSettingsFactory;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private ReleaseErrorReportingService releaseErrorReportingService;
    @Mock
    private SearchService searchService;
    @Mock
    private UserManager userManager;
    @Mock
    private Version version;
    @Mock
    private VersionManager versionManager;

    @InjectMocks
    private BambooReleaseServiceImpl service;

    @Test
    public void releasePermissionCheckShouldHandleNullApplicationUser() {
        // Set up
        setUpPermissions(null, true, true);

        // Invoke
        final boolean answer = service.hasPermissionToRelease((ApplicationUser) null, project);

        // Check
        assertThat(answer, is(true));
    }

    @Test
    public void releasePermissionCheckShouldHandleNullDirectoryUser() {
        // Set up
        setUpPermissions(null, true, true);

        // Invoke
        final boolean answer = service.hasPermissionToRelease((User) null, project);

        // Check
        assertThat(answer, is(true));
    }

    private void setUpPermissions(final ApplicationUser user, final boolean projectAdmin, final boolean globalAdmin) {
        when(permissionManager.hasPermission(ADMINISTER_PROJECTS, project, user)).thenReturn(projectAdmin);
    }

    @Test
    public void jiraReleaseShouldUseEnteredReleaseDate() {
        // Set up
        final String dateString = "theEnteredDateString";
        final Map<String, String> releaseConfig = singletonMap(PS_CONFIG_RELEASE_DATE, dateString);
        final Date enteredDate = mock(Date.class);
        final Version editedVersion = mock(Version.class);
        when(version.isReleased()).thenReturn(false);
        when(dateFieldFormat.parseDatePicker(dateString)).thenReturn(enteredDate);
        when(versionManager.editVersionReleaseDate(version, enteredDate)).thenReturn(editedVersion);

        // Invoke
        service.doJiraRelease(version, releaseConfig);

        // Check
        verify(versionManager).releaseVersion(editedVersion, true);
    }

    @Test
    public void getBambooVariablesFromMapShouldReplacePrefixAndIgnoreBlankValues() {
        // Set up
        final String prefixSubstitute = "myPrefix";
        final String goodKey = "good-key";
        final String goodValue = "non-blank-value";
        final Map<String, String> inputMap = ImmutableMap.of(
                "this-key-should-be-ignored", "this-value-should-be-ignored",
                VARIABLE_PARAM_PREFIX + goodKey, goodValue,
                VARIABLE_PARAM_PREFIX + "good-key-with-blank-value", "\t\r\n "
        );

        // Invoke
        final Map<String, String> bambooVariables = service.getBambooVariablesFromMap(inputMap, prefixSubstitute);

        // Check
        assertThat(bambooVariables.size(), is(1));
        assertThat(bambooVariables, hasEntry(prefixSubstitute + goodKey, goodValue));
    }
}
