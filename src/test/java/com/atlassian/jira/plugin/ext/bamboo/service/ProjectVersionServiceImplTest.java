package com.atlassian.jira.plugin.ext.bamboo.service;

import io.atlassian.fugue.Either;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.query.Query;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.jira.plugin.ext.bamboo.service.ProjectVersionServiceImpl.ADMIN_ERROR_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.service.ProjectVersionServiceImpl.UNLIMITED_FILTER;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProjectVersionServiceImplTest {
    private static final int UNRESOLVED_ISSUE_COUNT = 666;

    private static final long PROJECT_ID = 345;
    private static final long VERSION_ID = 678;

    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);

    @Mock
    private ApplicationUser applicationUser;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private SearchService searchService;
    @Mock
    private SearchResults searchResults;

    @InjectMocks
    private ProjectVersionServiceImpl service;

    @Before
    public void setUp() {
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(applicationUser);
        when(searchResults.getTotal()).thenReturn(UNRESOLVED_ISSUE_COUNT);
    }

    @Test
    public void shouldGetUnresolvedIssueCountSuccessfully() throws Exception {
        // Set up
        when(searchService.search(eq(applicationUser), any(Query.class), eq(UNLIMITED_FILTER)))
                .thenReturn(searchResults);

        // Invoke
        final Either<String, Integer> result = service.getUnresolvedIssueCount(PROJECT_ID, VERSION_ID);

        // Check
        assertThat(result.isRight(), is(true));
        assertThat(result.right().get(), is(UNRESOLVED_ISSUE_COUNT));
    }

    @Test
    public void shouldHandleExceptionWhenGettingUnresolvedIssueCount() throws Exception {
        // Set up
        final Throwable searchException = new SearchException("Just a test - please ignore");
        when(searchService.search(eq(applicationUser), any(Query.class), eq(UNLIMITED_FILTER)))
                .thenThrow(searchException);
        final String errorMessage = "foo";
        when(i18nHelper.getText(ADMIN_ERROR_KEY, searchException)).thenReturn(errorMessage);

        // Invoke
        final Either<String, Integer> result = service.getUnresolvedIssueCount(PROJECT_ID, VERSION_ID);

        // Check
        assertThat(result.isLeft(), is(true));
        assertThat(result.left().get(), is(errorMessage));
    }
}
