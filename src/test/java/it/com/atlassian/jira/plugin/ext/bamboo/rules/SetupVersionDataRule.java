package it.com.atlassian.jira.plugin.ext.bamboo.rules;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.restclient.Version;
import com.atlassian.jira.testkit.client.restclient.VersionClient;
import it.com.atlassian.jira.plugin.ext.bamboo.WebDriverUtils;
import org.joda.time.DateTime;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.atlassian.gzipfilter.org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Rule to setup a project with a version.
 */
public class SetupVersionDataRule extends TestWatcher {
    public static final String VERSION_NAME = "1.0";

    private String projectKey;
    private long projectId;
    private Version version;

    @Inject
    private JiraTestedProduct jira;

    @Inject
    protected Backdoor backdoor;

    @Override
    protected void finished(Description description) {
        deleteProjectAndVersion();
    }

    public Version getVersion() {
        return version;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public Version createVersionAndProject(final String username, final Locale locale) {
        projectKey = isNotBlank(projectKey) ? projectKey : WebDriverUtils.getNewProjectKey();
        projectId = projectId > 0 ? projectId : backdoor.project().addProject("Test " + projectKey, projectKey, username);

        Version versionData = createVersion(projectId, VERSION_NAME, new DateTime().minusDays(2).toDate(),
                new DateTime().plusDays(10).toDate(), locale);

        version = getVersionClient().create(versionData);
        assert version.id != null;

        return version;
    }

    private void deleteProjectAndVersion() {
        if (isNotBlank(projectKey)) {
            backdoor.project().deleteProject(projectKey);
        }
    }

    private VersionClient getVersionClient() {
        return new VersionClient(jira.environmentData());
    }

    private Version createVersion(Long projectId, String name, Date startDate, Date releaseDate, Locale locale) {
        // the formatted release dates depend on the Locale
        final DateFormat dateFormat = new SimpleDateFormat("d/MMM/yyyy", locale);
        return new Version().projectId(projectId)
                .name(name)
                .userStartDate(dateFormat.format(startDate))
                .userReleaseDate(dateFormat.format(releaseDate));
    }
}
