package it.com.atlassian.jira.plugin.ext.bamboo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import javax.annotation.Nonnull;
import java.util.List;

import static org.apache.commons.lang.RandomStringUtils.randomAlphabetic;

public final class WebDriverUtils {

    private WebDriverUtils() {
    }

    /**
     * Clicks the first element matching the given selector, if any.
     *
     * @param driver   the web driver
     * @param selector the selector
     */
    public static void clickFirstVisibleElement(@Nonnull final WebDriver driver, @Nonnull final By selector) {
        final List<WebElement> elements = driver.findElements(selector);
        for (final WebElement element : elements) {
            if (element.isDisplayed()) {
                element.click();
                return;
            }
        }
    }

    /**
     * Returns a random project key.
     *
     * @return an uppercase string of five alphabetic characters
     */
    @Nonnull
    public static String getNewProjectKey() {
        return randomAlphabetic(5).toUpperCase();
    }
}
