package it.com.atlassian.jira.plugin.ext.bamboo;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.project.versions.Version;
import com.atlassian.jira.pageobjects.project.versions.VersionPageTab;
import com.atlassian.jira.projects.pageobjects.webdriver.page.legacy.BrowseProjectPage;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.ResourceRef;
import com.atlassian.jira.testkit.client.IssuesControl;
import it.com.atlassian.jira.plugin.ext.bamboo.pageobjects.BambooBrowseVersionPage;
import it.com.atlassian.jira.plugin.ext.bamboo.pageobjects.BambooVersionsTab;
import it.com.atlassian.jira.plugin.ext.bamboo.pageobjects.ReleaseReportPage;

import javax.annotation.Nonnull;
import java.util.Date;
import java.util.Optional;

import static com.atlassian.jira.plugin.ext.bamboo.util.DayOfMonthMatcher.isSameDayOfMonth;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class WebDriverVersionUtils {
    public static ReleaseReportPage goToReleaseReportPageFor(final JiraTestedProduct jira,
                                                             final String projectKey, final Long versionId) {
        return jira.goTo(ReleaseReportPage.class, projectKey, versionId);
    }

    public static BambooBrowseVersionPage goToBrowseVersionPageFor(
            final JiraTestedProduct jira, final String projectKey, final String versionName) {
        return jira.goTo(BrowseProjectPage.class, projectKey)
                .openTab(BambooVersionsTab.class)
                .getVersion(versionName)
                .goToBrowseVersion();
    }

    public static void assertVersionReleased(
            final JiraTestedProduct jira, final String projectKey, final String versionName) {
        getVersionByName(jira, projectKey, versionName).ifPresent(version ->
                assertTrue("Version " + versionName + " for project " + projectKey + " should be released",
                        version.isReleased())
        );
    }

    public static void assertVersionNotReleased(
            final JiraTestedProduct jira, final String projectKey, final String versionName) {
        getVersionByName(jira, projectKey, versionName).ifPresent(version ->
                assertTrue("Version " + versionName + " for project " + projectKey + " should not be released",
                        !version.isReleased())
        );
    }

    public static void createNewVersion(final JiraTestedProduct jira, final String projectKey, final String version) {
        createNewVersion(jira, projectKey, version, "", "9/Jan/20");
    }

    public static void createNewVersion(@Nonnull final JiraTestedProduct jira, @Nonnull final String projectKey,
                                        @Nonnull final String versionName, final String startDate, final String releaseDate) {
        final VersionPageTab versionPageTab = jira.getPageBinder().navigateToAndBind(VersionPageTab.class, projectKey);
        versionPageTab.getEditVersionForm().fill(versionName, "A New Test Version", startDate, releaseDate).submit();
    }

    /**
     * Creates a new version and an issue assigned to that version.
     *
     * @param jira        the jira tested product
     * @param projectKey  the key of the project in which to create the version
     * @param versionName the name of the version to create
     * @param startDate   the start date for the version
     * @param releaseDate the release date for the version
     */
    public static void createNewVersionWithIssue(
            @Nonnull final JiraTestedProduct jira, @Nonnull final String projectKey,
            @Nonnull final String versionName, final String startDate, final String releaseDate) {
        createNewVersion(jira, projectKey, versionName, startDate, releaseDate);
        IssuesControl issuesControl = jira.backdoor().issues();
        String issueKey = issuesControl.createIssue(projectKey, "a summary").key();
        issuesControl.setIssueFields(issueKey, new IssueFields().fixVersions(new ResourceRef().name(versionName)));
    }

    public static void assertVersionReleaseDate(
            @Nonnull final JiraTestedProduct jira,
            @Nonnull final String projectKey,
            @Nonnull final String versionName,
            @Nonnull final Date date) {
        getVersionByName(jira, projectKey, versionName).ifPresent(version ->
                assertThat("The release date day is correct", version.getReleaseDate(), isSameDayOfMonth(date))
        );
    }

    private static Optional<Version> getVersionByName(
            @Nonnull final JiraTestedProduct jira, @Nonnull final String projectKey, @Nonnull final String versionName) {
        final VersionPageTab versionPageTab = jira.getPageBinder().navigateToAndBind(VersionPageTab.class, projectKey);
        return versionPageTab.getVersions().stream()
                .filter(version -> version.getName().equals(versionName))
                .findFirst();
    }
}
