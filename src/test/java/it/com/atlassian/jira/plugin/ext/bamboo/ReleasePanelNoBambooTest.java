package it.com.atlassian.jira.plugin.ext.bamboo;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.EnvironmentBasedProductInstance;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import it.com.atlassian.jira.plugin.ext.bamboo.pageobjects.ReleaseDialog;
import it.com.atlassian.jira.plugin.ext.bamboo.pageobjects.ReleaseManagementSection;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import java.util.Date;

import static it.com.atlassian.jira.plugin.ext.bamboo.WebDriverVersionUtils.assertVersionReleaseDate;
import static it.com.atlassian.jira.plugin.ext.bamboo.WebDriverVersionUtils.assertVersionReleased;
import static it.com.atlassian.jira.plugin.ext.bamboo.WebDriverVersionUtils.goToBrowseVersionPageFor;
import static it.com.atlassian.jira.plugin.ext.bamboo.utils.PrivacyPolicyAcknowledgeControl.acknowledgePrivacyPolicy;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Functional test to check that releasing a version with JBAM works correctly when Bamboo is not connected.
 */
@Ignore("Flaky, see https://extranet.atlassian.com/jira/browse/BDEV-7127")
public class ReleasePanelNoBambooTest {
    private static final String VERSION_1 = "1.0";
    private static final String RELEASE_REPORT_DARK_FEATURE = "jira.plugin.devstatus.phasefour.enabled";

    @Rule
    public WebDriverScreenshotRule screenshotRule = new WebDriverScreenshotRule();

    private static JiraTestedProduct jira = new JiraTestedProduct(new EnvironmentBasedProductInstance());

    private Backdoor backdoor;
    private String projectKey;

    @Before
    public void setup() {
        backdoor = new Backdoor(new TestKitLocalEnvironmentData());

        acknowledgePrivacyPolicy(jira);

        jira.gotoLoginPage().loginAndGoToHome("admin", "admin");

        projectKey = WebDriverUtils.getNewProjectKey();
        backdoor.project().addProject("Test " + projectKey, projectKey, "admin");
        backdoor.applicationProperties().setString(APKeys.JIRA_BASEURL, jira.getProductInstance().getBaseUrl());

        backdoor.darkFeatures().enableForSite(RELEASE_REPORT_DARK_FEATURE);
    }

    @After
    public void tearDown() {
        if (backdoor != null) {
            if (projectKey != null) {
                backdoor.project().deleteProject(projectKey);
            }
            backdoor.darkFeatures().disableForSite(RELEASE_REPORT_DARK_FEATURE);
        }
    }

    @Test
    public void testCanRelease() throws Exception {
        // setup

        // This is a workaround for FUSE-2010, whereby the JavaScript for the release report is
        // broken when the version has no issues. We ensure the version has at least one issue.
        WebDriverVersionUtils.createNewVersionWithIssue(jira, projectKey, VERSION_1, "", "");
        ReleaseManagementSection releaseManagementSection = openReleasePanelFor(VERSION_1);
        ReleaseDialog releaseDialog = releaseManagementSection.openReleaseDialog(false);
        releaseDialog.setReleaseDateToToday();

        // execute
        releaseDialog.submit(false);

        // check
        assertVersionReleased(jira, projectKey, VERSION_1);
        assertVersionReleaseDate(jira, projectKey, VERSION_1, new Date());
    }

    @Test
    public void testCannotReleaseIfReleaseDateBeforeStartDate() throws Exception {
        // setup
        WebDriverVersionUtils.createNewVersionWithIssue(jira, projectKey, VERSION_1, "9/Jan/20", "");
        ReleaseManagementSection releaseManagementSection = openReleasePanelFor(VERSION_1);
        ReleaseDialog releaseDialog = releaseManagementSection.openReleaseDialog(false);
        releaseDialog.setReleaseDateToToday();

        // execute
        releaseDialog.submitWithErrors();

        // check
        assertThat("release date < start date error is present in dialog",
                releaseDialog.hasReleaseDateError(), is(true));
        WebDriverVersionUtils.assertVersionNotReleased(jira, projectKey, VERSION_1);
    }

    private ReleaseManagementSection openReleasePanelFor(final String versionName) {
        return goToBrowseVersionPageFor(jira, projectKey, versionName).openTab(ReleaseManagementSection.class);
    }
}
