package it.com.atlassian.jira.plugin.ext.bamboo.rules;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import it.com.atlassian.jira.plugin.ext.bamboo.utils.PrivacyPolicyAcknowledgeControl;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.inject.Inject;

/**
 * Rule to acknowledge the privacy policy dialog once per class.
 */
public class PrivacyPolicyAcknowledgeRule extends TestWatcher {
    @Inject
    private JiraTestedProduct jira;

    @Override
    protected void starting(Description description) {
        PrivacyPolicyAcknowledgeControl.acknowledgePrivacyPolicy(jira);
    }
}
