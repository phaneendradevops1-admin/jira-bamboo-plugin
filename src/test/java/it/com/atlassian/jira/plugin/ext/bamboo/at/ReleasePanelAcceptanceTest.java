package it.com.atlassian.jira.plugin.ext.bamboo.at;

import com.atlassian.jira.testkit.client.restclient.Version;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.test.categories.OnDemandAcceptanceTest;
import it.com.atlassian.jira.plugin.ext.bamboo.pageobjects.ReleaseDialog;
import it.com.atlassian.jira.plugin.ext.bamboo.pageobjects.ReleaseManagementSection;
import it.com.atlassian.jira.plugin.ext.bamboo.rules.SetupVersionDataRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static it.com.atlassian.jira.plugin.ext.bamboo.WebDriverVersionUtils.goToReleaseReportPageFor;

/**
 * Basic release panel and dialog acceptance test to perform a Jira release.
 */
@Category(OnDemandAcceptanceTest.class)
public class ReleasePanelAcceptanceTest extends AcceptanceBaseTest {
    @Rule
    @Inject
    public SetupVersionDataRule setupVersionDataRule;

    private Version version;

    @Before
    public void setup() {
        final String username = getAdminUsername();
        version = setupVersionDataRule.createVersionAndProject(username, getJiraLocale(username));
    }

    @Test
    public void testShouldDoJiraRelease() throws Exception {
        final ReleaseManagementSection releaseButtonSection = openReleasePanelFor(setupVersionDataRule.getProjectKey(), version.id);
        Poller.waitUntilTrue("Release button should be visible", releaseButtonSection.isOpen());

        ReleaseDialog dialog = releaseButtonSection.openReleaseDialog(false);
        dialog.setReleaseDateToToday();
        dialog.selectNoBuild().submit(false);

        waitUntilFalse("Release button should not appear because version was released", releaseButtonSection.releaseButtonIsVisible());
    }

    private ReleaseManagementSection openReleasePanelFor(final String projectKey, final long versionId) {
        goToReleaseReportPageFor(jira, projectKey, versionId);
        return jira.getPageBinder().bind(ReleaseManagementSection.class);
    }
}