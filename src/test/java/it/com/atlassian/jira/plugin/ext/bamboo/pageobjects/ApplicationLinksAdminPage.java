package it.com.atlassian.jira.plugin.ext.bamboo.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraAdminPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.webdriver.utils.element.WebDriverPoller;
import it.com.atlassian.jira.plugin.ext.bamboo.pagecomponents.ual.AddApplicationLinkDialog;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import javax.inject.Inject;

import static com.atlassian.webdriver.utils.Check.elementIsVisible;
import static com.atlassian.webdriver.utils.element.ElementConditions.isNotVisible;
import static com.atlassian.webdriver.utils.element.ElementConditions.isVisible;

public class ApplicationLinksAdminPage extends AbstractJiraAdminPage {

    @ElementBy(id = "add-application-link")
    private PageElement addApplicationLink;

    @Inject
    private WebDriverPoller poller;

    @Override
    public String linkId() {
        return null;
    }

    public String getUrl() {
        return "/plugins/servlet/applinks/listApplicationLinks";
    }

    @Override
    public TimedCondition isAt() {
        return addApplicationLink.timed().isPresent();
    }

    public AddApplicationLinkDialog addApplicationLink() {
        addApplicationLink.click();
        poller.waitUntil(isVisible(By.id("add-application-link-dialog")));
        return pageBinder.bind(AddApplicationLinkDialog.class);
    }

    public void clearAllApplicationLinks() {
        if (elementIsVisible(By.id("add-first-application-link"), driver)) {
            return;
        }

        // TODO there can be a second step in the deletion process.
        poller.waitUntil(isVisible(By.id("application-links-table")));
        for (final WebElement appDeleteLink : driver.findElements(By.className("app-delete-link"))) {
            appDeleteLink.click();
            poller.waitUntil(isNotVisible(By.className("delete-applink-loading")));
            poller.waitUntil(isVisible(By.cssSelector("#delete-application-link-dialog .wizard-submit")));
            driver.findElement(By.cssSelector("#delete-application-link-dialog .wizard-submit")).click();
            poller.waitUntil(isNotVisible(By.id("delete-application-link-dialog")));
        }
    }
}
