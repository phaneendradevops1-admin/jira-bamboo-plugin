package it.com.atlassian.jira.plugin.ext.bamboo.pageobjects;

import com.atlassian.jira.projects.pageobjects.webdriver.page.legacy.AbstractProjectTab;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.openqa.selenium.By;

import javax.inject.Inject;

/**
 * 'Browse Versions' project tab,
 *
 * @since v4.4
 */
public class BambooVersionsTab extends AbstractProjectTab {
    public static String LINK_ID = PROJECT_PLUGIN_PREFIX + "versions-panel-panel";


    @ElementBy(id = "versions_panel")
    private PageElement versionsTable;

    @Inject
    private PageBinder pageBinder;

    public BambooVersionsTab(String projectKey) {
        super(LINK_ID, projectKey);
    }

    public Iterable<BambooVersionRow> getVersions() {
        return Iterables.transform(versionsTable.findAll(By.cssSelector("tbody > tr")), new Function<PageElement, BambooVersionRow>() {
            @Override
            public BambooVersionRow apply(PageElement from) {
                return pageBinder.bind(BambooVersionRow.class, projectKey, from);
            }
        });
    }

    public BambooVersionRow getVersion(final String versionName) {
        return Iterables.find(getVersions(), input -> input.getName().equals(versionName));

    }
}

