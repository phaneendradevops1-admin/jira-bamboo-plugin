package it.com.atlassian.jira.plugin.ext.bamboo.at;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.user.preferences.PreferenceKeys;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.webdriver.testing.annotation.TestedProductClass;
import com.atlassian.webdriver.testing.runner.ProductContextRunner;
import com.google.common.base.Preconditions;
import it.com.atlassian.jira.plugin.ext.bamboo.rules.AcceptanceTestBrowserRules;
import org.junit.Rule;
import org.junit.runner.RunWith;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.Locale;

import static it.com.atlassian.jira.plugin.ext.bamboo.utils.JiraLocaleUtil.fromJiraLocale;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@TestedProductClass(JiraTestedProduct.class)
@RunWith(ProductContextRunner.class)
public abstract class AcceptanceBaseTest {
    @Inject
    protected Backdoor backdoor;
    @Inject
    protected JiraTestedProduct jira;
    @Inject
    protected PageBinder pageBinder;

    @Rule
    @Inject
    public AcceptanceTestBrowserRules browserRules;

    // No protected rubbish helper methods and such please. Use test rules and the DI context associated  with
    // JiraTestedProduct, as demonstrated above.

    protected String getAdminUsername() {
        return jira.getAdminCredentials().getUsername();
    }

    protected Locale getJiraLocale(@Nonnull final String username) {
        Preconditions.checkNotNull(username);
        final String jiraLocale = backdoor.userProfile().preferences(username).getString(PreferenceKeys.USER_LOCALE);
        return isNotBlank(jiraLocale) ? fromJiraLocale(jiraLocale) : Locale.US;
    }
}
