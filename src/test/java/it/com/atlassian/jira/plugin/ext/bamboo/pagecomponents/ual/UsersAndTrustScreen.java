package it.com.atlassian.jira.plugin.ext.bamboo.pagecomponents.ual;

import com.atlassian.webdriver.utils.element.WebDriverPoller;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.inject.Inject;

import static com.atlassian.webdriver.utils.element.ElementConditions.isNotVisible;
import static it.com.atlassian.jira.plugin.ext.bamboo.WebDriverUtils.clickFirstVisibleElement;

public class UsersAndTrustScreen {
    @FindBy(css = "#add-application-link-dialog  #sameUser")
    private WebElement sameUsersRadio;
    @FindBy(css = "#add-application-link-dialog  #differentUser")
    private WebElement differentUsersRadio;
    @FindBy(css = "#add-application-link-dialog  #trusted")
    private WebElement trustedRadio;
    @FindBy(css = "#add-application-link-dialog  #notTrusted")
    private WebElement notTrustedRadio;

    @Inject
    private WebDriver driver;
    @Inject
    private WebDriverPoller poller;

    public void submitTrustDetails(final boolean sameUsers, final boolean trusted) {
        if (sameUsers) {
            sameUsersRadio.click();
        } else {
            differentUsersRadio.click();
        }

        if (trusted) {
            trustedRadio.click();
        } else {
            notTrustedRadio.click();
        }

        clickFirstVisibleElement(driver, By.className("wizard-submit"));
        poller.waitUntil(isNotVisible(By.className("add-application-link-dialog")));
    }
}
