package it.com.atlassian.jira.plugin.ext.bamboo.rest;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import javax.ws.rs.core.MediaType;

/**
 * Retrieves the results of the version operations REST API, used to drive the menu on the
 * Manage Versions page.
 */
public class VersionOperationsClient extends RestApiClient<VersionOperationsClient> {
    private final String rootPath;

    public VersionOperationsClient(final JIRAEnvironmentData environmentData) {
        super(environmentData);
        this.rootPath = environmentData.getBaseUrl().toExternalForm();
    }

    public Response<VersionOperations[]> getOperations(final String projectKey) throws UniformInterfaceException {
        return toResponse(() -> {
            return createVersionOperationsResource(projectKey).type(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);
        }, VersionOperations[].class);
    }

    private WebResource createVersionOperationsResource(final String projectKey) {
        return resourceRoot(rootPath)
                .path("rest")
                .path("api")
                .path("2")
                .path("project")
                .path(projectKey)
                .path("versions")
                .queryParam("expand", "operations");
    }
}
