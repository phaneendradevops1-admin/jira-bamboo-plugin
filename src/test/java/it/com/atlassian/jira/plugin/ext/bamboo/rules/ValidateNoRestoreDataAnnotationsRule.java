package it.com.atlassian.jira.plugin.ext.bamboo.rules;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.util.junit.AnnotatedDescription;
import com.atlassian.jira.pageobjects.config.ResetData;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import static java.lang.String.format;

/**
 * A JUnit rule that makes sure none of the {@link com.atlassian.integrationtesting.runner.restore.Restore} or
 * {@link com.atlassian.jira.pageobjects.config.ResetData} annotations is present on a test. Useful for AT/SLAT
 * environments where such operations are not permitted.
 */
public class ValidateNoRestoreDataAnnotationsRule implements TestRule {
    @Override
    public Statement apply(final Statement base, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                final AnnotatedDescription annotatedDescription = new AnnotatedDescription(description);
                if (annotatedDescription.isAnnotatedWith(Restore.class) || annotatedDescription.isAnnotatedWith(ResetData.class)) {
                    throw new AssertionError(format("Test %s is annotated with @Restore, or @ResetData, which is not allowed",
                            description.getDisplayName()));
                }
                base.evaluate();
            }
        };
    }
}
