package it.com.atlassian.jira.plugin.ext.bamboo.testkit;

import com.atlassian.applinks.core.rest.model.ApplicationLinkEntity;
import com.atlassian.applinks.core.rest.model.ApplicationLinkListEntity;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.sun.jersey.api.client.WebResource;

import javax.annotation.Nonnull;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static javax.ws.rs.core.HttpHeaders.ACCEPT;
import static javax.ws.rs.core.HttpHeaders.ACCEPT_CHARSET;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * A backdoor into app links features, to supplement those found in
 * {@link com.atlassian.jira.testkit.client.ApplicationLinkControl}.
 */
public class AppLinksClient extends com.atlassian.jira.testkit.client.BackdoorControl<AppLinksClient> {

    public AppLinksClient(final JIRAEnvironmentData jiraEnvironmentData) {
        super(jiraEnvironmentData);
    }

    /**
     * Returns the app links currently defined in Jira.
     *
     * @return see above
     */
    @Nonnull
    public List<ApplicationLinkEntity> getAppLinks() {
        return getBaseResource()
                .path("3.0").path("applicationlink")
                .header(ACCEPT, APPLICATION_JSON)
                .header(ACCEPT_CHARSET, UTF_8)
                .get(ApplicationLinkListEntity.class)
                .getApplications();
    }

    /**
     * Returns the base of the app links REST API.
     *
     * @return see above
     */
    @Nonnull
    private WebResource getBaseResource() {
        return resourceRoot(rootPath).path("rest").path("applinks");
    }
}
