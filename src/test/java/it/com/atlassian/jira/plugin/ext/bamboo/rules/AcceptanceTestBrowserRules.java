package it.com.atlassian.jira.plugin.ext.bamboo.rules;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.config.junit4.rule.DirtyWarningTerminatorRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.LoginRule;
import com.atlassian.jira.pageobjects.config.junit4.rule.RuleChainBuilder;
import com.atlassian.jira.pageobjects.config.junit4.rule.WebSudoRule;
import com.atlassian.webdriver.testing.rule.SessionCleanupRule;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.atlassian.webdriver.testing.rule.WindowSizeRule;

import javax.inject.Inject;

/**
 * Browser test rules for Acceptance Tests/SLATs, which has some specific restrictions:
 * <ul>
 * <li>data restore is not allowed</li>
 * </ul>
 */
public class AcceptanceTestBrowserRules extends AbstractRuleChainDecorator {
    @Inject
    public AcceptanceTestBrowserRules(JiraTestedProduct jira) {
        super(RuleChainBuilder.forProduct(jira)
                .around(ValidateNoRestoreDataAnnotationsRule.class)
                .around(SessionCleanupRule.class)
                .around(WindowSizeRule.class)
                .around(DirtyWarningTerminatorRule.class)
                .around(WebSudoRule.class)
                .around(WebDriverScreenshotRule.class)
                .around(LoginRule.class)
                //.around(PrivacyPolicyAcknowledgeRule.class) // rule is failed in Freezer
                .build());
    }
}
