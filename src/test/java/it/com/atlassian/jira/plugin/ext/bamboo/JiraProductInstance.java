package it.com.atlassian.jira.plugin.ext.bamboo;

import com.atlassian.pageobjects.ProductInstance;

/**
 * The Jira instance used by this project's integration tests.
 */
public final class JiraProductInstance implements ProductInstance {
    /**
     * The only instance of this class.
     */
    public static final JiraProductInstance INSTANCE = new JiraProductInstance();

    private static final String JIRA_PORT = "jira.port";
    private static final String JIRA_CONTEXT = "jira.context";

    private static final int PORT = Integer.parseInt(System.getProperty(JIRA_PORT, "1990"));
    private static final String CONTEXT = System.getProperty(JIRA_CONTEXT, "/jbam");

    private JiraProductInstance() {
    }

    public String getBaseUrl() {
        return "http://localhost:" + getHttpPort() + CONTEXT;
    }

    public int getHttpPort() {
        return PORT;
    }

    public String getContextPath() {
        return CONTEXT;
    }

    public String getInstanceId() {
        return "JIRA";
    }
}
