package it.com.atlassian.jira.plugin.ext.bamboo.utils;

public final class PluginKeys {
    public static final String FUSION_PLUGIN_KEY = "com.atlassian.jira.plugins.jira-development-integration-plugin";
    public static final String HIPCHAT_PLUGIN_KEY = "com.atlassian.labs.hipchat.hipchat-for-jira-plugins";
    public static final String SOFTWARE_PLUGIN_KEY = "com.atlassian.jira.jira-software-application";

    private PluginKeys() {
    }
}
