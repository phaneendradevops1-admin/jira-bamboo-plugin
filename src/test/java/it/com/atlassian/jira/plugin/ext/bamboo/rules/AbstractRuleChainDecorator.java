package it.com.atlassian.jira.plugin.ext.bamboo.rules;

import org.junit.rules.RuleChain;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

public class AbstractRuleChainDecorator implements TestRule {
    protected final RuleChain chain;

    protected AbstractRuleChainDecorator(@Nonnull RuleChain chain) {
        this.chain = checkNotNull(chain, "chain");
    }

    @Override
    public final Statement apply(Statement base, Description description) {
        return chain.apply(base, description);
    }
}
