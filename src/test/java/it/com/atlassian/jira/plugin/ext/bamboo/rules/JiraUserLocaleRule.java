package it.com.atlassian.jira.plugin.ext.bamboo.rules;

import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.user.preferences.PreferenceKeys;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.Locale;

import static com.google.common.base.Preconditions.checkNotNull;
import static it.com.atlassian.jira.plugin.ext.bamboo.utils.JiraLocaleUtil.fromJiraLocale;
import static it.com.atlassian.jira.plugin.ext.bamboo.utils.JiraLocaleUtil.toJiraLocale;

/**
 * Rule to change the user locale for the test.
 */
public class JiraUserLocaleRule extends TestWatcher {
    private String orginalLocale;
    private String username;

    @Inject
    private Backdoor backdoor;


    @Override
    protected void finished(Description description) {
        // restore the original locale
        if (orginalLocale != null) {
            changeUserLocale(username, orginalLocale);
            resetFields();
        }
    }

    public String getJiraLocaleString(@Nonnull final String username) {
        checkNotNull(username);
        return backdoor.userProfile().preferences(username).getString(PreferenceKeys.USER_LOCALE);
    }

    public Locale getJiraLocale(@Nonnull final String username) {
        checkNotNull(username);
        return fromJiraLocale(getJiraLocaleString(username));
    }

    /**
     * Set a new Locale for Jira for a given user
     */
    public void switchLocale(@Nonnull final Locale locale, @Nonnull final String username) {
        checkNotNull(locale);
        this.username = checkNotNull(username);
        this.orginalLocale = getJiraLocaleString(username);

        changeUserLocale(toJiraLocale(locale), username);
    }

    private void resetFields() {
        this.username = null;
        this.orginalLocale = null;
    }

    private void changeUserLocale(final String jiraLocale, final String username) {
        backdoor.userProfile().preferences(username).set(PreferenceKeys.USER_LOCALE, jiraLocale);
    }
}
