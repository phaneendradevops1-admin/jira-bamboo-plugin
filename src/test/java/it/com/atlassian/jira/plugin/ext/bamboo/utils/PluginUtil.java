package it.com.atlassian.jira.plugin.ext.bamboo.utils;

import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.plugin.PluginState;

import javax.annotation.Nonnull;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Test helper methods relating to plugins.
 */
public final class PluginUtil {
    /**
     * Asserts that the plugin with the given key is in the given state.
     *
     * @param backdoor      the backdoor for the Jira instance to check
     * @param pluginKey     the complete plugin key
     * @param expectedState the expected state
     */
    public static void assertPluginState(@Nonnull final Backdoor backdoor,
                                         @Nonnull final String pluginKey, @Nonnull final PluginState expectedState) {
        assertThat("Plugin state validation failed for " + pluginKey,
                backdoor.plugins().getPluginState(pluginKey), is(expectedState.name()));
    }

    private PluginUtil() {
    }
}
