package it.com.atlassian.jira.plugin.ext.bamboo.utils;

import com.atlassian.applinks.spi.application.TypeId;

/**
 * Contains various well-known types of app links.
 */
public final class AppLinkTypes {

    /**
     * The {@link com.atlassian.applinks.spi.application.TypeId} for Bamboo.
     */
    public static final TypeId BAMBOO = new TypeId("bamboo");

    private AppLinkTypes() {
    }
}
