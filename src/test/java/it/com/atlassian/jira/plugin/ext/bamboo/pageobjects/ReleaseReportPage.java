package it.com.atlassian.jira.plugin.ext.bamboo.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import static com.atlassian.pageobjects.elements.timeout.TimeoutType.AJAX_ACTION;

public class ReleaseReportPage extends AbstractJiraPage {
    private static final String URI_TEMPLATE = "/browse/%s/fixforversion/%s/?selectedTab=com.atlassian.jira.plugins.jira-development-integration-plugin:release-report-tabpanel";

    @ElementBy(id = "release-report")
    private PageElement releaseReportContainer;

    private final String uri;

    public ReleaseReportPage(final String projectKey, final Long versionId) {
        this.uri = String.format(URI_TEMPLATE, projectKey, versionId);
    }

    @Override
    public TimedCondition isAt() {
        return releaseReportContainer.withTimeout(AJAX_ACTION).timed().isPresent();
    }

    @Override
    public String getUrl() {
        return uri;
    }
}
