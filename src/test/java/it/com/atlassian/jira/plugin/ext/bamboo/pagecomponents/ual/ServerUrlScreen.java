package it.com.atlassian.jira.plugin.ext.bamboo.pagecomponents.ual;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.webdriver.utils.element.WebDriverPoller;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.inject.Inject;

import static com.atlassian.webdriver.utils.element.ElementConditions.isVisible;

public class ServerUrlScreen {
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = Logger.getLogger(ServerUrlScreen.class);

    @FindBy(id = "application-url")
    private WebElement serverUrlField;

    @FindBy(css = "#add-application-link-dialog .applinks-next-button")
    private WebElement button;

    @Inject
    private PageBinder pageBinder;

    @Inject
    private WebDriverPoller poller;

    public ReciprocalUrlScreen submitServerUrl(final String url) {
        serverUrlField.sendKeys(url);
        button.click();

        // wait for new screen
        poller.waitUntil(isVisible((By.cssSelector("#add-application-link-dialog #reciprocalLink"))));

        return pageBinder.bind(ReciprocalUrlScreen.class);
    }
}
